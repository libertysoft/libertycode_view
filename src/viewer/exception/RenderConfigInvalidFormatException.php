<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\exception;

use Exception;

use liberty_code\view\viewer\library\ConstViewer;



class RenderConfigInvalidFormatException extends Exception
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor
    // ******************************************************************************

    /**
     * Constructor
     *
     * @param mixed $config
     */
    public function __construct($config)
    {
        // Call parent constructor
        parent::__construct();

        // Init var
        $this->message = sprintf
        (
            ConstViewer::EXCEPT_MSG_RENDER_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
    }





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache required option
            (
                (!isset($config[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid argument add required option
            (
                (!isset($config[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE]) ||
                    is_int($config[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE]) ||
                    (
                        is_string($config[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE]) &&
                        ctype_digit($config[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    public static function setCheck($config)
    {
        // Init var
        $result =
            is_null($config) ||
            (
                // Check valid array
                is_array($config) &&
                static::checkConfigIsValid($config)
            );

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}