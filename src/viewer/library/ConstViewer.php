<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\library;



class ConstViewer
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';
    const DATA_KEY_DEFAULT_ADD_ARG_DATA = 'objAddArgData';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_KEY_PATTERN = 'cache_key_pattern';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';
    const TAB_CONFIG_KEY_ARGUMENT_ADD_REQUIRE = 'argument_add_require';

    // Configuration render
    const TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE = 'argument_add_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default viewer configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be a cache repository object.';
    const EXCEPT_MSG_DATA_INVALID_FORMAT = 'Following data "%1$s" invalid! It must be null or a table data object.';
    const EXCEPT_MSG_RENDER_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default viewer rendering configuration standard.';
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a string, not empty.';
    const EXCEPT_MSG_KEY_NOT_FOUND = 'Following key "%1s" not found!';



}