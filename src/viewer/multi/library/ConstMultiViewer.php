<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\multi\library;



class ConstMultiViewer
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_PROVIDER = 'objProvider';



    // Configuration
    const TAB_CONFIG_KEY_VIEWER = 'viewer';
    const TAB_CONFIG_KEY_SELECT_KEY_REGEXP = 'select_key_regexp';
    const TAB_CONFIG_KEY_SELECT_KEY_PREFIX = 'select_key_prefix';
    const TAB_CONFIG_KEY_ORDER = 'order';
    const TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE = 'select_viewer_first_require';



    // Exception message constants
    const EXCEPT_MSG_PROVIDER_INVALID_FORMAT = 'Following DI provider "%1$s" invalid! It must be a provider object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the multi viewer configuration standard.';
    const EXCEPT_MSG_VIEWER_NOT_FOUND = 'Viewer not found, for following key "%1$s"!';



}