<?php
/**
 * Description :
 * This class allows to define multi viewer class.
 * Multi viewer is default viewer,
 * using list of viewers, to get specified render.
 *
 * Multi viewer uses the following specified configuration:
 * [
 *     Default viewer configuration,
 *
 *     viewer(required): [
 *         // Viewer 1
 *         object viewer interface 1
 *
 *         OR
 *
 *         "string viewer dependency key|class path"
 *
 *         [
 *             viewer(required):
 *                 object viewer interface 1
 *                 OR
 *                 "string viewer dependency key|class path",
 *
 *             select_key_regexp(optional: no REGEXP selection done, if not found):
 *                 "string REGEXP pattern,
 *                 used to select render keys, which match with following REGEXP pattern,
 *                 available for specific viewer",
 *
 *             select_key_prefix(optional: no prefix selection done, if not found):
 *                 "string prefix,
 *                 used to select render keys, which start with following prefix,
 *                 available for specific viewer"
 *                 OR
 *                 ["string prefix 1", ..., "string prefix N"],
 *
 *             order(optional: 0 got if not found): integer
 *         ],
 *
 *         ...,
 *
 *         // Viewer N
 *         ...
 *     ],
 *
 *     select_viewer_first_require(optional: got false if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\multi\model;

use liberty_code\view\viewer\model\DefaultViewer;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\data\data\table\model\TableData;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\view\viewer\library\ConstViewer;
use liberty_code\view\viewer\api\ViewerInterface;
use liberty_code\view\viewer\multi\library\ConstMultiViewer;
use liberty_code\view\viewer\multi\exception\ProviderInvalidFormatException;
use liberty_code\view\viewer\multi\exception\ConfigInvalidFormatException;
use liberty_code\view\viewer\multi\exception\ViewerNotFoundException;



/**
 * @method null|ProviderInterface getObjProvider() Get provider object.
 * @method void setObjProvider(null|ProviderInterface $objProvider) Set provider object.
 */
class MultiViewer extends DefaultViewer
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider = null
     */
    public function __construct(
        array $tabConfig = null,
        ProviderInterface $objProvider = null,
        RepositoryInterface $objCacheRepo = null,
        TableData $objAddArgData = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo,
            $objAddArgData
        );

        // Init provider
        $this->setObjProvider($objProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMultiViewer::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstMultiViewer::DATA_KEY_DEFAULT_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMultiViewer::DATA_KEY_DEFAULT_PROVIDER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMultiViewer::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                case ConstViewer::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check select viewer first option required,
     * used to select viewer, on same order.
     *
     * @return boolean
     */
    public function checkSelectViewerFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Init var
        $objViewer = $this->getObjViewer($strKey);
        $result = (!is_null($objViewer));

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of viewer configurations.
     *
     * @param string $strKey = null
     * @param null|boolean $sortAsc = null
     * @return array
     */
    protected function getTabViewerConfig($strKey = null, $sortAsc = null)
    {
        // Init var
        $strKey = (is_string($strKey) ? $strKey : null);
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);
        $tabConfig = $this->getTabConfig();
        $result = array_values($tabConfig[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER]);

        // Filter viewer configurations, if required
        if(!is_null($strKey))
        {
            $checkKeySelectedFromPrefix = function($strKey, $prefix)
            {
                $result = false;
                $tabPrefix = (is_array($prefix) ? array_values($prefix) : array($prefix));

                // Run each prefix
                for($intCpt = 0; ($intCpt < count($tabPrefix)) && (!$result); $intCpt++)
                {
                    // Check key starts with prefix
                    $strPrefix = $tabPrefix[$intCpt];
                    $result = ToolBoxString::checkStartsWith($strKey, $strPrefix);
                }

                return $result;
            };

            $result = array_filter(
                $result,
                function($viewerConfig) use ($checkKeySelectedFromPrefix, $strKey) {
                    return (
                        (!is_array($viewerConfig)) ||
                        (
                            (
                                (!array_key_exists(ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_REGEXP, $viewerConfig)) ||
                                (preg_match($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_REGEXP], $strKey) == 1)
                            ) &&
                            (
                                (!array_key_exists(ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX, $viewerConfig)) ||
                                $checkKeySelectedFromPrefix($strKey, $viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX])
                            )
                        )
                    );
                }
            );
        }

        // Sort viewer configurations, if required
        if(is_bool($boolSortAsc))
        {
            usort(
                $result,
                function($viewerConfig1, $viewerConfig2)
                {
                    $intOrder1 = (
                        (
                            is_array($viewerConfig1) &&
                            array_key_exists(ConstMultiViewer::TAB_CONFIG_KEY_ORDER, $viewerConfig1)
                        ) ?
                            $viewerConfig1[ConstMultiViewer::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $intOrder2 = (
                        (
                            is_array($viewerConfig2) &&
                            array_key_exists(ConstMultiViewer::TAB_CONFIG_KEY_ORDER, $viewerConfig2)
                        ) ?
                            $viewerConfig2[ConstMultiViewer::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get viewer object,
     * from specified string render key.
     *
     * @param string $strKey
     * @return null|ViewerInterface
     */
    protected function getObjViewer($strKey)
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();
        $tabConfig = $this->getTabConfig();
        $tabViewerConfig = $this->getTabViewerConfig($strKey, true);
        $boolSelectFirst = $this->checkSelectViewerFirstRequired();

        // Run each viewer configuration
        $intOrderRef = null;
        $boolOrderValid = true;
        for(
            $intCpt = 0;
            ($intCpt < count($tabViewerConfig)) &&
            (
                is_null($result) ||
                (!$boolSelectFirst)
            ) &&
            $boolOrderValid;
            $intCpt++
        )
        {
            // Get info
            $viewerConfig = $tabViewerConfig[$intCpt];
            $intOrder = (
                (
                    is_array($viewerConfig) &&
                    array_key_exists(ConstMultiViewer::TAB_CONFIG_KEY_ORDER, $viewerConfig)
                ) ?
                    $viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_ORDER] :
                    0
            );
            $boolOrderValid = (is_null($intOrderRef) || ($intOrderRef == $intOrder));

            // Get and register viewer, if required
            if($boolOrderValid)
            {
                // Get viewer
                $objViewer = (
                    (
                        is_array($viewerConfig) &&
                        array_key_exists(ConstMultiViewer::TAB_CONFIG_KEY_VIEWER, $viewerConfig)
                    ) ?
                        $viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER] :
                        $viewerConfig
                );
                $objViewer = (
                    (
                        is_string($objViewer) &&
                        (!is_null($objProvider))
                    ) ?
                        $objProvider->get($objViewer) :
                        $objViewer
                );

                // Check valid viewer
                if(!($objViewer instanceof ViewerInterface))
                {
                    throw new ConfigInvalidFormatException(serialize($tabConfig));
                }

                // Register viewer, if required
                if($objViewer->checkExists($strKey))
                {
                    // Init order reference, if required
                    $intOrderRef = (is_null($intOrderRef) ? $intOrder : $intOrderRef);

                    // Register viewer
                    $result = $objViewer;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ViewerNotFoundException
     */
    protected function getStrRenderEngine(
        $strKey,
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Init var
        $objViewer = $this->getObjViewer($strKey);

        // Check viewer found
        if(is_null($objViewer))
        {
            throw new ViewerNotFoundException($strKey);
        }

        // Return result
        return $objViewer->getStrRender(
            $strKey,
            $tabArg,
            $tabConfig
        );
    }



}