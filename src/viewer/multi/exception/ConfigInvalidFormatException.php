<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\multi\exception;

use Exception;

use liberty_code\view\viewer\api\ViewerInterface;
use liberty_code\view\viewer\multi\library\ConstMultiViewer;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiViewer::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init viewer configuration array check function
        $checkTabViewerConfigIsValid = function($tabViewerConfig) use ($checkTabStrIsValid)
        {
            $result = is_array($tabViewerConfig) && (count($tabViewerConfig) > 0);

            // Check each viewer configuration valid, if required
            if($result)
            {
                $tabViewerConfig = array_values($tabViewerConfig);
                for($intCpt = 0; ($intCpt < count($tabViewerConfig)) && $result; $intCpt++)
                {
                    $viewerConfig = $tabViewerConfig[$intCpt];
                    $result = (
                        // Check valid viewer
                        ($viewerConfig instanceof ViewerInterface) ||

                        (
                            is_string($viewerConfig) &&
                            (trim($viewerConfig) != '')
                        ) ||

                        (
                            is_array($viewerConfig) &&

                            // Check valid viewer
                            isset($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER]) &&
                            (
                                ($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER] instanceof ViewerInterface) ||
                                (
                                    is_string($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER]) &&
                                    (trim($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER]) != '')
                                )
                            ) &&

                            // Check valid selection key REGEXP pattern
                            (
                                (!isset($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_REGEXP])) ||
                                (
                                    is_string($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_REGEXP]) &&
                                    (trim($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_REGEXP]) != '')
                                )
                            ) &&

                            // Check valid selection key prefix
                            (
                                (!isset($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX])) ||
                                (
                                    is_string($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX]) &&
                                    (trim($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX]) != '')
                                ) ||
                                (
                                    is_array($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX]) &&
                                    $checkTabStrIsValid($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_KEY_PREFIX])
                                )
                            ) &&

                            // Check valid order
                            (
                                (!isset($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_ORDER])) ||
                                is_int($viewerConfig[ConstMultiViewer::TAB_CONFIG_KEY_ORDER])
                            )
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid viewer
            isset($config[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER]) &&
            $checkTabViewerConfigIsValid($config[ConstMultiViewer::TAB_CONFIG_KEY_VIEWER]) &&

            // Check valid select viewer first required option
            (
                (!isset($config[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE]) ||
                    is_int($config[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstMultiViewer::TAB_CONFIG_KEY_SELECT_VIEWER_FIRST_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}