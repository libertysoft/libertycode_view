<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\multi\exception;

use Exception;

use liberty_code\view\viewer\multi\library\ConstMultiViewer;



class ViewerNotFoundException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param string $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstMultiViewer::EXCEPT_MSG_VIEWER_NOT_FOUND, strval($strKey));
	}



}