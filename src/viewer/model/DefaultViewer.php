<?php
/**
 * Description :
 * This class allows to define default viewer class.
 * Can be consider is base of all viewer types.
 *
 * Default viewer uses the following specified configuration:
 * [
 *     cache_require(optional: got true if not found): true / false,
 *
 *     cache_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on cache repository, where '%1$s' replaced by specified render hash",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ],
 *
 *     argument_add_require(optional: got true if not found):
 *         true / false,
 *         specify if adding arguments required, to customize specified render
 * ]
 *
 * Note:
 * -> Additional argument data format:
 *     - Data source is array.
 *     - Key format: string|integer key, used as argument key.
 *     - Value format: mixed|callable, used to get argument value.
 *     If callable: mixed function(
 *         mixed $key,
 *         ViewerInterface $objViewer,
 *         array $tabArg = array()
 *         array $tabConfig = null
 *     ).
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\view\viewer\api\ViewerInterface;

use Closure;
use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\data\data\table\model\TableData;
use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\viewer\library\ConstViewer;
use liberty_code\view\viewer\exception\ConfigInvalidFormatException;
use liberty_code\view\viewer\exception\CacheRepoInvalidFormatException;
use liberty_code\view\viewer\exception\DataInvalidFormatException;
use liberty_code\view\viewer\exception\RenderConfigInvalidFormatException;
use liberty_code\view\viewer\exception\KeyNotFoundException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method null|TableData getObjAddArgData() Get additional argument data object.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 * @method void setObjAddArgData(null|TableData $objAddArgData) Set additional argument data object. Table data format: @see DefaultViewer additional argument data format.
 */
abstract class DefaultViewer extends FixBean implements ViewerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     * @param TableData $objAddArgData = null
     */
	public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null,
        TableData $objAddArgData = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }

        // Init additional argument data, if required
        if(!is_null($objAddArgData))
        {
            $this->setObjAddArgData($objAddArgData);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstViewer::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstViewer::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstViewer::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstViewer::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }

        if(!$this->beanExists(ConstViewer::DATA_KEY_DEFAULT_ADD_ARG_DATA))
        {
            $this->__beanTabData[ConstViewer::DATA_KEY_DEFAULT_ADD_ARG_DATA] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstViewer::DATA_KEY_DEFAULT_CONFIG,
            ConstViewer::DATA_KEY_DEFAULT_CACHE_REPO,
            ConstViewer::DATA_KEY_DEFAULT_ADD_ARG_DATA
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstViewer::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstViewer::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;

                case ConstViewer::DATA_KEY_DEFAULT_ADD_ARG_DATA:
                    DataInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * Configuration array format:
     * Rendering configuration can be provided.
     * @see getStrRender() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws RenderConfigInvalidFormatException
     */
    public function checkCacheRequired(array $tabConfig = null)
    {
        // Set check argument
        RenderConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabRenderConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstViewer::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstViewer::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabRenderConfig[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabRenderConfig[ConstViewer::TAB_RENDER_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check adding arguments required,
     * to customize specified render.
     *
     * Configuration array format:
     * Rendering configuration can be provided.
     * @see getStrRender() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkArgAddRequired(array $tabConfig = null)
    {
        // Init var
        $tabRenderConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjAddArgData())) &&
            (
                (!isset($tabConfig[ConstViewer::TAB_CONFIG_KEY_ARGUMENT_ADD_REQUIRE])) ||
                (intval($tabConfig[ConstViewer::TAB_CONFIG_KEY_ARGUMENT_ADD_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabRenderConfig[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE])) ||
                (intval($tabRenderConfig[ConstViewer::TAB_RENDER_CONFIG_KEY_ARGUMENT_ADD_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string hash,
     * from specified render key.
     *
     * @param string $strKey
     * @param array $tabData = array()
     * @return string
     */
    protected function getStrHash(
        $strKey,
        array $tabData = array()
    )
    {
        // Return result
        return
            $strKey .
            ToolBoxHash::getStrHash($tabData, true);
    }



    /**
     * Get specified render cache key,
     * from specified render key.
     *
     * @param string $strKey
     * @param array $tabData = array()
     * @return string
     */
    protected function getStrCacheKey(
        $strKey,
        array $tabData = array()
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getStrHash($strKey, $tabData);
        $result = (
            array_key_exists(ConstViewer::TAB_CONFIG_KEY_CACHE_KEY_PATTERN, $tabConfig) ?
                sprintf($tabConfig[ConstViewer::TAB_CONFIG_KEY_CACHE_KEY_PATTERN], $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get specified array of formatted arguments,
     * used to customize specified render.
     * Overwrite it to implement specific feature.
     *
     * Arguments array format:
     * @see getStrRender() arguments array format.
     *
     * Configuration array format:
     * @see getStrRender() configuration array format.
     *
     * @param array $tabArg = array()
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabArgFormat(
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Init var
        $result = $tabArg;

        // Add additional arguments, if required
        if($this->checkArgAddRequired($tabConfig))
        {
            // Run each additional argument
            $tabAddArg = $this
                ->getObjAddArgData()
                ->getDataSrc();
            foreach($tabAddArg as $key => $addArg)
            {
                // Add additional argument, if required
                if(!array_key_exists($key, $result))
                {
                    // Get info
                    $addArg = (
                        ($addArg instanceof Closure) ?
                            $addArg($key, $this, $tabArg, $tabConfig) :
                            $addArg
                    );

                    // Register additional argument
                    $result[$key] = $addArg;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified string render engine.
     *
     * Arguments array format:
     * @see getStrRender() arguments array format.
     *
     * Configuration array format:
     * @see getStrRender() configuration array format.
     *
     * @param string $strKey
     * @param array $tabArg = array()
     * @param array $tabConfig = null
     * @return string
     */
    abstract protected function getStrRenderEngine(
        $strKey,
        array $tabArg = array(),
        array $tabConfig = null
    );



    /**
     * Configuration array format:
     * [
     *     cache_require(optional: got true if not found): true / false,
     *
     *     argument_add_require(optional: got true if not found):
     *         true / false,
     *         specify if adding arguments required, to customize specified render
     * ]
     *
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function getStrRender(
        $strKey,
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Set check argument
        if(!$this->checkExists($strKey))
        {
            throw new KeyNotFoundException($strKey);
        }

        // Init var
        $tabRenderConfig = $tabConfig;
        $tabArg = $this->getTabArgFormat(
            $tabArg,
            $tabRenderConfig
        );
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired($tabRenderConfig) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheKey($strKey, $tabArg),
                    function() use ($strKey, $tabArg, $tabRenderConfig) {
                        return $this->getStrRenderEngine(
                            $strKey,
                            $tabArg,
                            $tabRenderConfig
                        );
                    },
                    (
                        isset($tabConfig[ConstViewer::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstViewer::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getStrRenderEngine(
                    $strKey,
                    $tabArg,
                    $tabRenderConfig
                )
        );

        // Return result
        return $result;
    }



}