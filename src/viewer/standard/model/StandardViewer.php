<?php
/**
 * Description :
 * This class allows to define standard viewer class.
 * Standard viewer is default viewer,
 * using template repository and compiler, to get specified render.
 *
 * Standard viewer uses the following specified configuration:
 * [
 *     Default viewer configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\standard\model;

use liberty_code\view\viewer\model\DefaultViewer;

use liberty_code\data\data\table\model\TableData;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\api\CompilerInterface;
use liberty_code\view\viewer\standard\library\ConstStandardViewer;
use liberty_code\view\viewer\standard\exception\TemplateRepoInvalidFormatException;
use liberty_code\view\viewer\standard\exception\CompilerInvalidFormatException;
use liberty_code\view\viewer\standard\exception\RenderConfigInvalidFormatException;



/**
 * @method TmpRepositoryInterface getObjTemplateRepo() Get template repository object.
 * @method CompilerInterface getObjCompiler() Get compiler object.
 * @method void setObjTemplateRepo(TmpRepositoryInterface $objTemplateRepo) Set template repository object.
 * @method void setObjCompiler(CompilerInterface $objCompiler) Set compiler object.
 */
class StandardViewer extends DefaultViewer
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param TmpRepositoryInterface $objTemplateRepo
     * @param CompilerInterface $objCompiler
     */
	public function __construct(
        TmpRepositoryInterface $objTemplateRepo,
        CompilerInterface $objCompiler,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null,
        TableData $objAddArgData = null
    )
	{
		// Call parent constructor
		parent::__construct(
            $tabConfig,
            $objCacheRepo,
            $objAddArgData
        );

        // Init template repository
        $this->setObjTemplateRepo($objTemplateRepo);

        // Init compiler
        $this->setObjCompiler($objCompiler);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardViewer::DATA_KEY_DEFAULT_TEMPLATE_REPO))
        {
            $this->__beanTabData[ConstStandardViewer::DATA_KEY_DEFAULT_TEMPLATE_REPO] = null;
        }

        if(!$this->beanExists(ConstStandardViewer::DATA_KEY_DEFAULT_COMPILER))
        {
            $this->__beanTabData[ConstStandardViewer::DATA_KEY_DEFAULT_COMPILER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardViewer::DATA_KEY_DEFAULT_TEMPLATE_REPO,
            ConstStandardViewer::DATA_KEY_DEFAULT_COMPILER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardViewer::DATA_KEY_DEFAULT_TEMPLATE_REPO:
                    TemplateRepoInvalidFormatException::setCheck($value);
                    break;

                case ConstStandardViewer::DATA_KEY_DEFAULT_COMPILER:
                    CompilerInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists($strKey)
    {
        // Return result
        return $this
            ->getObjTemplateRepo()
            ->checkExists($strKey);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Configuration array format:
     * [
     *     @see getStrRender() configuration array format,
     *
     *     compiler(optional): [
     *         @see CompilerInterface::getStrCompileRender() configuration array format.
     *     ]
     * ]
     *
     * @inheritdoc
     * @throws RenderConfigInvalidFormatException
     */
    protected function getStrRenderEngine(
        $strKey,
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Set check argument
        RenderConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabRenderConfig = $tabConfig;
        $result = $this
            ->getObjTemplateRepo()
            ->getStrContent($strKey);
        $result = $this
            ->getObjCompiler()
            ->getStrCompileRender(
                $result,
                $tabArg,
                (
                    isset($tabRenderConfig[ConstStandardViewer::TAB_RENDER_CONFIG_KEY_COMPILER]) ?
                        $tabRenderConfig[ConstStandardViewer::TAB_RENDER_CONFIG_KEY_COMPILER] :
                        null
                )
            );

        // Return result
        return $result;
    }



}