<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\standard\library;



class ConstStandardViewer
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_TEMPLATE_REPO = 'objTemplateRepo';
    const DATA_KEY_DEFAULT_COMPILER = 'objCompiler';



    // Configuration compile
    const TAB_RENDER_CONFIG_KEY_COMPILER = 'compiler';



    // Exception message constants
    const EXCEPT_MSG_TEMPLATE_REPO_INVALID_FORMAT = 'Following template repository "%1$s" invalid! It must be a template repository object.';
    const EXCEPT_MSG_COMPILER_INVALID_FORMAT = 'Following compiler "%1$s" invalid! It must be a compiler object.';
    const EXCEPT_MSG_RENDER_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the standard viewer rendering configuration standard.';



}