<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\standard\exception;

use Exception;

use liberty_code\view\compiler\api\CompilerInterface;
use liberty_code\view\viewer\standard\library\ConstStandardViewer;



class CompilerInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $compiler
     */
	public function __construct($compiler)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardViewer::EXCEPT_MSG_COMPILER_INVALID_FORMAT,
            mb_strimwidth(strval($compiler), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified compiler has valid format.
	 * 
     * @param mixed $compiler
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($compiler)
    {
		// Init var
		$result = (
            (!is_null($compiler)) &&
			($compiler instanceof CompilerInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($compiler);
		}
		
		// Return result
		return $result;
    }
	
	
	
}