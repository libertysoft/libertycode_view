<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/template/repository/test/TmpRepositoryTest.php');
require_once($strRootAppPath . '/src/compiler/test/CompilerTest.php');

// Use
use liberty_code\data\data\table\model\TableData;
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\view\viewer\standard\model\StandardViewer;
use liberty_code\view\viewer\multi\model\MultiViewer;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init standard viewer 1
$objStdViewer1 = new StandardViewer(
    $objRegisterTmpRepo,
    $objPhpCompiler,
    array(
        'cache_key_pattern' => 'view-saved-%1$s'
    ),
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'std_viewer_1',
        'source' => StandardViewer::class,
        'set' =>  ['type' => 'instance', 'value' => $objStdViewer1],
        'option' => [
            'shared' => true
        ]
    )));

//  Init standard viewer 2
$objStdViewer2 = new StandardViewer(
    $objCacheTmpRepo,
    $objPhpCompiler,
    array(
        'cache_key_pattern' => 'view-saved-%1$s'
    ),
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'key' => 'std_viewer_2',
        'source' => StandardViewer::class,
        'set' =>  ['type' => 'instance', 'value' => $objStdViewer2],
        'option' => [
            'shared' => true
        ]
    )));

// Init additional argument data
$objAddArgData = new TableData();

// Init multi viewer
$objMultiViewer = new MultiViewer(
    array(
        //'cache_key_pattern' => 'view-multi-saved-%1$s',
        'viewer' => [
            $objStdViewer1,
            [
                'viewer' => 'std_viewer_2',
                'select_key_prefix' => 'key-cache',
                'order' => -1
            ]
        ],
        //'select_viewer_first_require' => true
    ),
    $objProvider,
    null,//$objCacheRepo,
    $objAddArgData
);


