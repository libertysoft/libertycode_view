<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/viewer/test/ViewerTest.php');



// Init var
$objAddArgData->putValue('appVersion', function() {return phpversion();});
$objAddArgData->putValue('appNm', 'Test');



// Init template repositories
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-1', '<strong>Template 1</strong><p>Hello <?= $firstNm; ?> - <?= $nm; ?></p>');
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-cache-2', '<strong>Template 2</strong><p>Hello word</p>');

$objCacheTmpRepo->getObjRepository()->setItem('tmp-key-cache-2', '<strong>Template cache 2</strong><p>App info: <?= $appNm; ?> (v<?= $appVersion; ?>)</p><p>Hello word</p>');
$objCacheTmpRepo->getObjRepository()->setItem('tmp-key-cache-3', '<strong>Template cache 3</strong><p>App info: <?= $appNm; ?> (v<?= $appVersion; ?>)</p><p>Hello <?= $firstNm; ?> - <?= $nm; ?></p>');



// Test config property
echo('Test viewer 1 config: <br />');

try{
    $objStdViewer1->setTabConfig(array('cache_key_pattern' => 7));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test viewer 1 config: <pre>');var_dump($objStdViewer1->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



echo('Test viewer 2 config: <br />');

try{
    $objStdViewer2->setTabConfig(array('cache_key_pattern' => true));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test viewer 2 config: <pre>');var_dump($objStdViewer2->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



echo('Test multi viewer config: <br />');

try{
    $objMultiViewer->setTabConfig(array('viewer' => [false]));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test multi viewer config: <pre>');var_dump($objMultiViewer->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test check, get render
$tabRender = array(
    [
        'key 1',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ko: not found
    [
        'key-1',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok: found: "Template 1 ..."
    [
        'test-cache-2',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ko: not found (viewer not found too)
    [
        'key-cache-2',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok: found: "Template cache 2"
    [
        'key-cache-3',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        [
            'cache_require' => 'test',
            'compiler' => [
                'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
            ]
        ]
    ], // Ko: bad rendering configuration format
    [
        'key-cache-3',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        [
            'cache_require' => false,
            'compiler' => [
                'php_render_pattern' => 7
            ]
        ]
    ], // Ko: bad compiling configuration format
    [
        'key-cache-3',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        [
            'cache_require' => false,
            'compiler' => [
                'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
            ]
        ]
    ] // Ok: found: "Template cache 3 ..."

);

foreach($tabRender as $render)
{
    $strKey = $render[0];
    $tabArg = $render[1];
    $tabConfig = $render[2];

    echo('Test check, get render"'.$strKey.'": <br />');
    try{
        echo('Check exists: <pre>');var_dump($objMultiViewer->checkExists($strKey));echo('</pre>');
        echo('Get render: <pre>');var_dump($objMultiViewer->getStrRender($strKey, $tabArg, $tabConfig));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test cache
echo('Test cache: <br />');
echo('<pre>');print_r($objCacheRepo->getTabItem($objCacheRepo->getTabSearchKey()));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


