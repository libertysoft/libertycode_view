<?php
/**
 * Description :
 * This class allows to describe behavior of viewer class.
 * Viewer allows to get specified render.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\viewer\api;



interface ViewerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified render exists.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkExists($strKey);





	// Methods getters
	// ******************************************************************************

    /**
     * Get specified string render.
     *
     * Arguments array format:
     * Array of arguments can be provided,
     * to customize specified render.
     *
     * Configuration array format:
     * Rendering configuration can be provided,
     * to configure rendering.
     * Null means no specific rendering configuration required.
     *
     * @param string $strKey
     * @param array $tabArg = array()
     * @param array $tabConfig = null
     * @return string
     */
    public function getStrRender(
        $strKey,
        array $tabArg = array(),
        array $tabConfig = null
    );



}