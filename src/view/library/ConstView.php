<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\library;



class ConstView
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_VIEWER = 'objViewer';
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';



    // Configuration
    const TAB_CONFIG_KEY_KEY = 'key';
    const TAB_CONFIG_KEY_ARGUMENT = 'argument';
    const TAB_CONFIG_KEY_RENDER = 'render';

    // Hash configuration
    const HASH_PATTERN = 'default_view_%1$s';



    // Exception message constants
    const EXCEPT_MSG_VIEWER_INVALID_FORMAT = 'Following viewer "%1$s" invalid! It must be a viewer object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default view configuration standard.';



}