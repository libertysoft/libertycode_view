<?php
/**
 * Description :
 * This class allows to describe behavior of view factory class.
 * View factory allows to provide new or specified view instance,
 * hydrated with a specified configuration,
 * from a set of potential predefined view types.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\factory\api;

use liberty_code\view\view\api\ViewInterface;



interface ViewFactoryInterface
{
    // ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get string class path of view,
     * from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return null|string
     */
    public function getStrViewClassPath(array $tabConfig, $strConfigKey = null);



    /**
     * Get new or specified view object,
     * hydrated from specified configuration.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @param ViewInterface $objView = null
     * @return null|ViewInterface
     */
    public function getObjView(
        array $tabConfig,
        $strConfigKey = null,
        ViewInterface $objView = null
    );



}