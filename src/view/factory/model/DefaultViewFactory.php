<?php
/**
 * Description :
 * This class allows to define default view factory class.
 * Can be consider is base of all view factory type.
 *
 * Default view factory uses the following specified configuration, to get and hydrate view:
 * [
 *     type(optional): "string constant to determine view type",
 *
 *     ... specific view configuration
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\factory\model;

use liberty_code\di\factory\model\DefaultFactory;
use liberty_code\view\view\factory\api\ViewFactoryInterface;

use liberty_code\library\reflection\library\ToolBoxReflection;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\view\viewer\api\ViewerInterface;
use liberty_code\view\view\api\ViewInterface;
use liberty_code\view\view\exception\ViewerInvalidFormatException;
use liberty_code\view\view\factory\library\ConstViewFactory;
use liberty_code\view\view\factory\exception\FactoryInvalidFormatException;
use liberty_code\view\view\factory\exception\ConfigInvalidFormatException;



/**
 * @method ViewerInterface getObjViewer() Get viewer object.
 * @method void setObjViewer(ViewerInterface $objViewer) Set viewer object.
 * @method null|ViewFactoryInterface getObjFactory() Get parent factory object.
 * @method void setObjFactory(null|ViewFactoryInterface $objFactory) Set parent factory object.
 */
abstract class DefaultViewFactory extends DefaultFactory implements ViewFactoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ViewerInterface $objViewer
     * @param ViewFactoryInterface $objFactory = null
     */
    public function __construct(
        ViewerInterface $objViewer,
        ViewFactoryInterface $objFactory = null,
        ProviderInterface $objProvider = null
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init viewer
        $this->setObjViewer($objViewer);

        // Init view factory
        $this->setObjFactory($objFactory);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Overwrite hydrateDefault().
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstViewFactory::DATA_KEY_DEFAULT_VIEWER))
        {
            $this->__beanTabData[ConstViewFactory::DATA_KEY_DEFAULT_VIEWER] = null;
        }

        if(!$this->beanExists(ConstViewFactory::DATA_KEY_DEFAULT_FACTORY))
        {
            $this->__beanTabData[ConstViewFactory::DATA_KEY_DEFAULT_FACTORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstViewFactory::DATA_KEY_DEFAULT_VIEWER,
            ConstViewFactory::DATA_KEY_DEFAULT_FACTORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstViewFactory::DATA_KEY_DEFAULT_VIEWER:
                    ViewerInvalidFormatException::setCheck($value);
                    break;

                case ConstViewFactory::DATA_KEY_DEFAULT_FACTORY:
                    FactoryInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate specified view.
     * Overwrite it to set specific call hydration.
     *
     * @param ViewInterface $objView
     * @param array $tabConfigFormat
     */
    protected function hydrateView(ViewInterface $objView, array $tabConfigFormat)
    {
        // Init formatted configuration
        if(array_key_exists(ConstViewFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat))
        {
            unset($tabConfigFormat[ConstViewFactory::TAB_CONFIG_KEY_TYPE]);
        }

        // Hydrate view
        $objViewer = $this->getObjViewer();
        $objView->setViewer($objViewer);

        $objView->setConfig($tabConfigFormat);
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if the specified formatted configuration is valid,
     * for the specified view object.
     *
     * @param ViewInterface $objView
     * @param array $tabConfigFormat
     * @return boolean
     */
    protected function checkConfigIsValid(ViewInterface $objView, array $tabConfigFormat)
    {
        // Init var
        $strViewClassPath = $this->getStrViewClassPathEngine($tabConfigFormat);
        $result = (
            (!is_null($strViewClassPath)) &&
            ($strViewClassPath == get_class($objView))
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatted configuration array.
     * Overwrite it to set specific feature.
     *
     * @param array $tabConfig
     * @param string $strConfigKey = null
     * @return array
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        return $tabConfig;
    }



    /**
     * Get string configured type,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     * @throws ConfigInvalidFormatException
     */
    protected function getStrConfigType(array $tabConfigFormat)
    {
        // Check arguments
        ConfigInvalidFormatException::setCheck($tabConfigFormat);

        // Init var
        $result = (
            array_key_exists(ConstViewFactory::TAB_CONFIG_KEY_TYPE, $tabConfigFormat) ?
                $tabConfigFormat[ConstViewFactory::TAB_CONFIG_KEY_TYPE] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * Get string class path of view,
     * from specified configured type.
     *
     * @param null|string $strConfigType
     * @return null|string
     */
    abstract protected function getStrViewClassPathFromType($strConfigType);



    /**
     * Get string class path of view engine,
     * from specified formatted configuration.
     *
     * @param array $tabConfigFormat
     * @return null|string
     */
    protected function getStrViewClassPathEngine(array $tabConfigFormat)
    {
        // Init var
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $result = $this->getStrViewClassPathFromType($strConfigType);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrViewClassPath(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getStrViewClassPathEngine($tabConfigFormat);

        // Get class path from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getStrViewClassPath($tabConfig, $strConfigKey);
        }

        // Return result
        return $result;
    }



    /**
     * Get new object instance view,
     * from specified configured type.
     * Overwrite it to set specific feature.
     *
     * @param null|string $strConfigType
     * @return null|ViewInterface
     */
    protected function getObjViewNew($strConfigType)
    {
        // Init var
        $strClassPath = $this->getStrViewClassPathFromType($strConfigType);

        // Init instance
        $result = $this->getObjInstance($strClassPath);
        if(is_null($result))
        {
            $result = ToolBoxReflection::getObjInstance($strClassPath);
        }

        // Return result
        return $result;
    }



    /**
     * Get object instance view engine.
     *
     * @param array $tabConfigFormat
     * @param ViewInterface $objView = null
     * @return null|ViewInterface
     */
    protected function getObjViewEngine(
        array $tabConfigFormat,
        ViewInterface $objView = null
    )
    {
        // Init var
        $result = null;
        $strConfigType = $this->getStrConfigType($tabConfigFormat);
        $objView = (
            is_null($objView) ?
                $this->getObjViewNew($strConfigType) :
                $objView
        );

        // Get and hydrate view, if required
        if(
            (!is_null($objView)) &&
            $this->checkConfigIsValid($objView, $tabConfigFormat)
        )
        {
            $this->hydrateView($objView, $tabConfigFormat);
            $result = $objView;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getObjView(
        array $tabConfig,
        $strConfigKey = null,
        ViewInterface $objView = null
    )
    {
        // Init var
        $tabConfigFormat = $this->getTabConfigFormat($tabConfig, $strConfigKey);
        $result = $this->getObjViewEngine($tabConfigFormat, $objView);

        // Get view from parent factory, if required
        $objFactory = $this->getObjFactory();
        if(
            (!is_null($objFactory)) &&
            is_null($result)
        )
        {
            $result = $objFactory->getObjView($tabConfig, $strConfigKey, $objView);
        }

        // Return result
        return $result;
    }



}