<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/view/factory/test/ViewFactoryTest.php');

// Use
use liberty_code\view\view\model\DefaultView;



// Test new view
$tabViewData = array(
    [
        [
            'key' => 'key-1',
            'argument' =>  [
                'firstNm' => 'John 1',
                'nm' => 'DOE 1'
            ],
            'render' => [
                'cache_require' => false
            ]
        ],
        'key-1-not-care'
    ], // Ok

    [
        [
            'type' => 'default',
            'argument' =>  [
                'firstNm' => 'John 2',
                'nm' => 'DOE 2'
            ],
            'render' => [
                'cache_require' => false
            ]
        ],
        'key-2'
    ], // Ok

    [
        [
            'argument' =>  [
                'firstNm' => 'John 3',
                'nm' => 'DOE 3'
            ],
            'render' => [
                'cache_require' => false
            ]
        ],
        'key-3'
    ], // Ok

    [
        [
            'type' => 'test',
            'key' => 'key-4',
            'argument' =>  [
                'firstNm' => 'John 4',
                'nm' => 'DOE 4'
            ],
            'render' => [
                'cache_require' => false
            ]
        ],
        'key-4-not-care'
    ], // Ko: default view config provided for unknown view

    [
        [
            'argument' =>  [
                'firstNm' => 'John 5',
                'nm' => 'DOE 5'
            ],
            'render' => [
                'cache_require' => false
            ]
        ],
        'key-5',
        new DefaultView($objMultiViewer)
    ] // Ok
);

foreach($tabViewData as $viewData)
{
    echo('Test new view: <br />');
    echo('<pre>');var_dump($viewData);echo('</pre>');

    try{
        $tabConfig = $viewData[0];
        $strConfigKey = (isset($viewData[1]) ? $viewData[1] : null);
        $objView = (isset($viewData[2]) ? $viewData[2] : null);
        $objView = $objViewFactory->getObjView($tabConfig, $strConfigKey, $objView);

        echo('Class path: <pre>');var_dump($objViewFactory->getStrViewClassPath($tabConfig, $strConfigKey));echo('</pre>');

        if(!is_null($objView))
        {
            echo('View class path: <pre>');var_dump(get_class($objView));echo('</pre>');
            echo('View config: <pre>');var_dump($objView->getTabConfig());echo('</pre>');
        }
        else
        {
            echo('View not found<br />');
        }

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . ':' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


