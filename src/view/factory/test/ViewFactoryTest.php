<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load test
require_once($strRootAppPath . '/src/viewer/test/ViewerTest.php');

// Use
use liberty_code\view\view\factory\standard\model\StandardViewFactory;



// Init standard view factory
$objViewFactory = new StandardViewFactory($objMultiViewer);


