<?php
/**
 * Description :
 * This class allows to define standard view factory class.
 * Standard view factory allows to provide and hydrate view instance.
 *
 * Standard view factory uses the following specified configuration, to get and hydrate view:
 * [
 *     -> Configuration key(optional): "string view key"
 *     type(required): "default",
 *     Default view configuration (@see DefaultView )
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\factory\standard\model;

use liberty_code\view\view\factory\model\DefaultViewFactory;

use liberty_code\view\view\library\ConstView;
use liberty_code\view\view\model\DefaultView;
use liberty_code\view\view\factory\standard\library\ConstStandardViewFactory;



class StandardViewFactory extends DefaultViewFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabConfigFormat(array $tabConfig, $strConfigKey = null)
    {
        // Init var
        $result = parent::getTabConfigFormat($tabConfig, $strConfigKey);

        // Format configuration, if required
        if(!is_null($strConfigKey))
        {
            // Add configured key as view key, if required
            if(!array_key_exists(ConstView::TAB_CONFIG_KEY_KEY, $result))
            {
                $result[ConstView::TAB_CONFIG_KEY_KEY] = $strConfigKey;
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrViewClassPathFromType($strConfigType)
    {
        // Init var
        $result = null;

        // Get class path of view, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardViewFactory::CONFIG_TYPE_DEFAULT:
                $result = DefaultView::class;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getObjViewNew($strConfigType)
    {
        // Init var
        $result = null;
        $objViewer = $this->getObjViewer();

        // Get view, from type
        switch($strConfigType)
        {
            case null:
            case ConstStandardViewFactory::CONFIG_TYPE_DEFAULT:
                $result = new DefaultView($objViewer);
                break;
        }

        // Return result
        return $result;
    }



}