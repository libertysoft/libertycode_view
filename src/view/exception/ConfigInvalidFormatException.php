<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\exception;

use Exception;

use liberty_code\view\view\library\ConstView;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstView::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid key
            (
                (!isset($config[ConstView::TAB_CONFIG_KEY_KEY])) ||
                (
                    is_string($config[ConstView::TAB_CONFIG_KEY_KEY]) &&
                    (trim($config[ConstView::TAB_CONFIG_KEY_KEY]) != '')
                )
            ) &&

            // Check valid array of arguments
            (
                (!isset($config[ConstView::TAB_CONFIG_KEY_ARGUMENT])) ||
                is_array($config[ConstView::TAB_CONFIG_KEY_ARGUMENT])
            ) &&

            // Check valid rendering configuration
            (
                (!isset($config[ConstView::TAB_CONFIG_KEY_RENDER])) ||
                is_array($config[ConstView::TAB_CONFIG_KEY_RENDER])
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}