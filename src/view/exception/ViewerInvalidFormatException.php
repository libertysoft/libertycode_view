<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\exception;

use Exception;

use liberty_code\view\viewer\api\ViewerInterface;
use liberty_code\view\view\library\ConstView;



class ViewerInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $viewer
     */
	public function __construct($viewer)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstView::EXCEPT_MSG_VIEWER_INVALID_FORMAT,
            mb_strimwidth(strval($viewer), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified viewer has valid format.
	 * 
     * @param mixed $viewer
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($viewer)
    {
		// Init var
		$result = (
            (!is_null($viewer)) &&
			($viewer instanceof ViewerInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($viewer);
		}
		
		// Return result
		return $result;
    }
	
	
	
}