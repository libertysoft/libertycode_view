<?php
/**
 * Description :
 * This class allows to define default view class.
 * Can be consider is base of all view types.
 *
 * Default view uses the following specified configuration:
 * [
 *     key(optional: got @see getStrHash() if not found): "string view key",
 *
 *     argument(optional: got [] if not found): [
 *          @see ViewerInterface::getStrRender() arguments array format.
 *     ],
 *
 *     render(optional: got null if not found): [
 *         @see ViewerInterface::getStrRender() configuration array format.
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\view\view\api\ViewInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\view\viewer\api\ViewerInterface;
use liberty_code\view\view\library\ConstView;
use liberty_code\view\view\exception\ViewerInvalidFormatException;
use liberty_code\view\view\exception\ConfigInvalidFormatException;



class DefaultView extends FixBean implements ViewInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param ViewerInterface $objViewer
     * @param array $tabConfig = null
     */
	public function __construct(
        ViewerInterface $objViewer,
        array $tabConfig = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init viewer
        $this->setViewer($objViewer);

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstView::DATA_KEY_DEFAULT_VIEWER))
        {
            $this->__beanTabData[ConstView::DATA_KEY_DEFAULT_VIEWER] = null;
        }

        if(!$this->beanExists(ConstView::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstView::DATA_KEY_DEFAULT_CONFIG] = array();
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstView::DATA_KEY_DEFAULT_VIEWER,
            ConstView::DATA_KEY_DEFAULT_CONFIG
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstView::DATA_KEY_DEFAULT_VIEWER:
                    ViewerInvalidFormatException::setCheck($value);
                    break;

                case ConstView::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkExists()
    {
        // Return result
        return $this
            ->getObjViewer()
            ->checkExists($this->getStrKey());
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getObjViewer()
    {
        // Return result
        return $this->beanGet(ConstView::DATA_KEY_DEFAULT_VIEWER);
    }



    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstView::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Get string hash.
     *
     * @return string
     */
    public function getStrHash()
    {
        // Return result
        return sprintf(ConstView::HASH_PATTERN, ToolBoxHash::getStrHash($this));
    }



    /**
     * @inheritdoc
     */
    public function getStrKey()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstView::TAB_CONFIG_KEY_KEY]) ?
                $tabConfig[ConstView::TAB_CONFIG_KEY_KEY] :
                $this->getStrHash()
        );;

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabArg()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstView::TAB_CONFIG_KEY_ARGUMENT]) ?
                $tabConfig[ConstView::TAB_CONFIG_KEY_ARGUMENT] :
                array()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabRenderConfig()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstView::TAB_CONFIG_KEY_RENDER]) ?
                $tabConfig[ConstView::TAB_CONFIG_KEY_RENDER] :
                null
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getStrRender()
    {
        // Return result
        return $this
            ->getObjViewer()
            ->getStrRender(
                $this->getStrKey(),
                $this->getTabArg(),
                $this->getTabRenderConfig()
            );
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setViewer(ViewerInterface $objViewer)
    {
        $this->beanSet(ConstView::DATA_KEY_DEFAULT_VIEWER, $objViewer);
    }



    /**
     * @inheritdoc
     */
    public function setConfig(array $tabConfig)
    {
        $this->beanSet(ConstView::DATA_KEY_DEFAULT_CONFIG, $tabConfig);
    }



    /**
     * Set specified string key,
     * used to retrieve render.
     *
     * @param string $strKey
     */
    public function setKey($strKey)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set key
        $tabConfig[ConstView::TAB_CONFIG_KEY_KEY] = $strKey;

        // Set configuration
        $this->setConfig($tabConfig);
    }



    /**
     * Set specified array of arguments,
     * used to customize render.
     *
     * @param array $tabArg = array()
     */
    public function setArg(array $tabArg = array())
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set array of arguments
        $tabConfig[ConstView::TAB_CONFIG_KEY_ARGUMENT] = $tabArg;

        // Set configuration
        $this->setConfig($tabConfig);
    }



    /**
     * Set rendering configuration array,
     * used to configure rendering.
     *
     * @param array $tabRenderConfig = null
     */
    public function setRenderConfig(array $tabRenderConfig = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();

        // Set rendering configuration, if required
        if(!is_null($tabRenderConfig))
        {
            $tabConfig[ConstView::TAB_CONFIG_KEY_RENDER] = $tabRenderConfig;
        }
        // Unset rendering configuration, if required
        else if(isset($tabConfig[ConstView::TAB_CONFIG_KEY_RENDER]))
        {
            unset($tabConfig[ConstView::TAB_CONFIG_KEY_RENDER]);
        }

        // Set configuration
        $this->setConfig($tabConfig);
    }



}