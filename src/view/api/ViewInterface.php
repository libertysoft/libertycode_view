<?php
/**
 * Description :
 * This class allows to describe behavior of view class.
 * View contains all information to get specific render,
 * using specific viewer.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\view\api;

use liberty_code\view\viewer\api\ViewerInterface;



interface ViewInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if render exists.
     *
     * @return boolean
     */
    public function checkExists();





	// Methods getters
	// ******************************************************************************

    /**
     * Get viewer object.
     *
     * @return ViewerInterface
     */
    public function getObjViewer();



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get string key (considered as view id),
     * used to retrieve render.
     *
     * @return string
     */
    public function getStrKey();



    /**
     * Get array of arguments,
     * used to customize render.
     *
     * @return array
     */
    public function getTabArg();



    /**
     * Get rendering configuration array,
     * used to configure rendering.
     *
     * @return null|array
     */
    public function getTabRenderConfig();



    /**
     * Get string render.
     *
     * @return string
     */
    public function getStrRender();





    // Methods setters
    // ******************************************************************************

    /**
     * Set viewer object.
     *
     * @param ViewerInterface $objViewer
     */
    public function setViewer(ViewerInterface $objViewer);



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     */
    public function setConfig(array $tabConfig);



}