<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/viewer/test/ViewerTest.php');

// Use
use liberty_code\view\view\model\DefaultView;



// Init var
$objAddArgData->putValue('appVersion', function() {return phpversion();});
$objAddArgData->putValue('appNm', 'Test');

// Init template repositories
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-1', '<strong>Template 1</strong><p>Hello <?= $firstNm; ?> - <?= $nm; ?></p>');
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-2', '<strong>Template 2</strong><p>App info: <?= $appNm; ?> (v<?= $appVersion; ?>)</p><p>Hello word</p>');
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-3', '<strong>Template 3</strong><p>App info: <?= $appNm; ?> (v<?= $appVersion; ?>)</p><p>Hello <?= $firstNm; ?> - <?= $nm; ?></p>');



// Test get view
$tabConfig = array(
    [
        'key' => 7,
        'argument' => [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ]
    ], // Ko: bad key format
    [
        'key' => 'key-1',
        'argument' => [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ]
    ], // Ok: found: "Template 1 ..."
    [
        'key' =>'test-2'
    ], // Ko: render not found
    [
        'key' =>'key-2'
    ], // Ok: found: "Template 2 ..."
    [
        'key' => 'key-3',
        'argument' => 'test',
        'render' => [
            'cache_require' => false,
            'compiler' => [
                'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
            ]
        ]
    ], // Ko: bad argument format
    [
        'key' => 'key-3',
        'argument' => [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        'render' => 'test'
    ], // Ko: bad rendering configuration format
    [
        'key' => 'key-3',
        'argument' =>  [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        'render' => [
            'cache_require' => false,
            'compiler' => [
                'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
            ]
        ]
    ], // Ok: found: "Template 3 ..."
    [], // Ko: render not found
    null // Ko: render not found
);

foreach($tabConfig as $config)
{
    echo('Test get view: <br />');
    echo('Config: <pre>');var_dump($config);echo('</pre>');
    try{
        $objView = new DefaultView($objMultiViewer, $config);
        echo('Get config: <pre>');var_dump($objView->getTabConfig());echo('</pre>');
        echo('Get key: <pre>');var_dump($objView->getStrKey());echo('</pre>');
        echo('Get arguments: <pre>');var_dump($objView->getTabArg());echo('</pre>');
        echo('Get rendering config: <pre>');var_dump($objView->getTabRenderConfig());echo('</pre>');
        echo('Check exists: <pre>');var_dump($objView->checkExists());echo('</pre>');
        echo('Get render: <pre>');var_dump($objView->getStrRender());echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test cache
echo('Test cache: <br />');
echo('<pre>');print_r($objCacheRepo->getTabItem($objCacheRepo->getTabSearchKey()));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test set view
$tabConfig = array(
    [
        7,
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        [
            'cache_require' => false,
            'compiler' => [
                'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
            ]
        ]
    ], // Ko: bad key format
    [
        'key-1',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok
    [
        'key-1',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        [
            'cache_require' => false,
            'compiler' => [
                'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
            ]
        ]
    ] // Ok
);

foreach($tabConfig as $config)
{
    $strKey = $config[0];
    $tabArg = $config[1];
    $tabRenderConfig = $config[2];

    echo('Test set view: <br />');
    echo('Config: <pre>');var_dump($config);echo('</pre>');
    try{
        $objView = new DefaultView($objMultiViewer);
        $objView->setKey($strKey);
        $objView->setArg($tabArg);
        $objView->setRenderConfig($tabRenderConfig);

        echo('Get config: <pre>');var_dump($objView->getTabConfig());echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');


