<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\php\exception;

use Exception;

use liberty_code\view\compiler\php\library\ConstPhpCompiler;



class ArgInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $arg
     */
	public function __construct($arg)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstPhpCompiler::EXCEPT_MSG_ARG_INVALID_FORMAT,
            mb_strimwidth(strval($arg), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified array of arguments has valid format.
     *
     * @param array $tabArg
     * @return boolean
     */
    protected static function checkTabArgIsValid(array $tabArg)
    {
        // Init var
        $result = true;

        // Run each argument
        $tabKey = array_keys($tabArg);
        for($intCpt = 0; ($intCpt < count($tabKey)) && $result; $intCpt++)
        {
            // Get info
            $key = $tabKey[$intCpt];

            // Check valid argument
            $result =
                // Check valid key
                is_string($key) && (trim($key) != '') &&
                (preg_match('#^[a-zA-Z_]\w*$#', $key) == 1);
            ;
        }

        // Return result
        return $result;
    }



	/**
	 * Check if specified array of arguments has valid format.
	 * 
     * @param mixed $arg
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($arg)
    {
		// Init var
		$result =
            // Check valid array
            is_array($arg) &&

            // Check valid array of arguments
            static::checkTabArgIsValid($arg);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($arg) ? serialize($arg) : $arg));
		}
		
		// Return result
		return $result;
    }
	
	
	
}