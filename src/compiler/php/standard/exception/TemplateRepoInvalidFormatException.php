<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\php\standard\exception;

use Exception;

use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\php\standard\library\ConstStandardPhpCompiler;



class TemplateRepoInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $repository
     */
	public function __construct($repository)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstStandardPhpCompiler::EXCEPT_MSG_TEMPLATE_REPO_INVALID_FORMAT,
            mb_strimwidth(strval($repository), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified template repository has valid format.
	 * 
     * @param mixed $repository
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($repository)
    {
		// Init var
		$result = (
            (!is_null($repository)) &&
			($repository instanceof TmpRepositoryInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($repository);
		}
		
		// Return result
		return $result;
    }
	
	
	
}