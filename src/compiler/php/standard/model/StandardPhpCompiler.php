<?php
/**
 * Description :
 * This class allows to define standard PHP compiler class.
 * Standard PHP compiler is PHP compiler,
 * using standard formatting and features, to get specific compiled render.
 *
 * Standard PHP compiler uses the following specified configuration:
 * [
 *     PHP compiler configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\php\standard\model;

use liberty_code\view\compiler\php\model\PhpCompiler;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\format\model\FormatData;
use liberty_code\view\compiler\format\library\ConstFormat;
use liberty_code\view\compiler\format\library\ToolBoxFormatTmpExtension;
use liberty_code\view\compiler\format\library\ToolBoxFormatTmpInclusion;
use liberty_code\view\compiler\php\standard\library\ConstStandardPhpCompiler;
use liberty_code\view\compiler\php\standard\exception\TemplateRepoInvalidFormatException;



/**
 * @method TmpRepositoryInterface getObjTemplateRepo() Get template repository object.
 * @method void setObjTemplateRepo(TmpRepositoryInterface $objTemplateRepo) Set template repository object.
 */
class StandardPhpCompiler extends PhpCompiler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
     * @param TmpRepositoryInterface $objTemplateRepo
     */
	public function __construct(
        TmpRepositoryInterface $objTemplateRepo,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null,
        FormatData $objFormatDataBefore = null,
        FormatData $objFormatDataAfter = null
    )
	{
		// Call parent constructor
		parent::__construct(
            $tabConfig,
            $objCacheRepo,
            $objFormatDataBefore,
            $objFormatDataAfter
        );

        // Init template repository
        $this->setObjTemplateRepo($objTemplateRepo);
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstStandardPhpCompiler::DATA_KEY_DEFAULT_TEMPLATE_REPO))
        {
            $this->__beanTabData[ConstStandardPhpCompiler::DATA_KEY_DEFAULT_TEMPLATE_REPO] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstStandardPhpCompiler::DATA_KEY_DEFAULT_TEMPLATE_REPO
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstStandardPhpCompiler::DATA_KEY_DEFAULT_TEMPLATE_REPO:
                    TemplateRepoInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get formatting configuration array,
     * used in template extension formatting.
     * Overwrite it to implement specific configuration.
     *
     * Array return format:
     * @see ToolBoxFormatTmpExtension::getStrRenderFormat() array of configuration format.
     *
     * @return array
     */
    protected function getTabFormatConfigTmpExtension()
    {
        // Return result
        return array(
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_TEMPLATE_EXT_REGEXP =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_EXT_TEMPLATE_EXT_REGEXP,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_REGEXP =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DCL_REGEXP,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_SPLIT =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DCL_SPLIT,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DEF_REGEXP =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DEF_REGEXP,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DEF_SPLIT =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DEF_SPLIT,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_PARENT_REGEXP =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_EXT_DEPENDENCY_PARENT_REGEXP,
        );
    }



    /**
     * Get specified string formatted render,
     * using template extension formatting,
     * before compiling.
     *
     * @param string $strRender
     * @return string
     */
    protected function getStrCompileRenderFormatTmpExtension($strRender)
    {
        // Return result
        return ToolBoxFormatTmpExtension::getStrRenderFormat(
            $strRender,
            $this->getObjTemplateRepo(),
            $this->getTabFormatConfigTmpExtension()
        );
    }



    /**
     * Get formatting configuration array,
     * used in template inclusion formatting.
     * Overwrite it to implement specific configuration.
     *
     * Array return format:
     * @see ToolBoxFormatTmpInclusion::getStrRenderFormat() array of configuration format.
     *
     * @return array
     */
    protected function getTabFormatConfigTmpInclusion()
    {
        // Return result
        return array(
            ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_INC_REGEXP =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_INC_TEMPLATE_INC_REGEXP
        );
    }



    /**
     * Get specified string formatted render,
     * using template inclusion formatting,
     * before compiling.
     *
     * @param string $strRender
     * @return string
     */
    protected function getStrCompileRenderFormatTmpInclusion($strRender)
    {
        // Return result
        return ToolBoxFormatTmpInclusion::getStrRenderFormat(
            $strRender,
            $this->getObjTemplateRepo(),
            $this->getTabFormatConfigTmpInclusion()
        );
    }



    /**
     * Get formatting configuration array,
     * used in template compilation formatting.
     * Overwrite it to implement specific configuration.
     *
     * Configuration array format:
     * @see getStrCompileRenderBase() configuration array format.
     *
     * Array return format:
     * @see ToolBoxFormatTmpInclusion::getStrRenderFormat() array of configuration format.
     *
     * @param array $tabConfig = null
     * @return array
     */
    protected function getTabFormatConfigTmpCompilation(array $tabConfig = null)
    {
        // Return result
        return array(
            ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_INC_REGEXP =>
                ConstStandardPhpCompiler::FORMAT_CONFIG_TMP_COMP_TEMPLATE_COMP_REGEXP,
            ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_CONTENT_FORMAT_CALLABLE =>
                function($strKey, $strContent) use ($tabConfig) {
                    return $this->getStrCompileRenderFormatBefore($strContent, $tabConfig);
                }
        );
    }



    /**
     * Get specified string formatted render,
     * using template compilation formatting,
     * before compiling.
     *
     * Configuration array format:
     * @see getStrCompileRenderBase() configuration array format.
     *
     * @param string $strRender
     * @param array $tabConfig = null
     * @return string
     */
    protected function getStrCompileRenderFormatTmpCompilation(
        $strRender,
        array $tabConfig = null
    )
    {
        // Return result
        return ToolBoxFormatTmpInclusion::getStrRenderFormat(
            $strRender,
            $this->getObjTemplateRepo(),
            $this->getTabFormatConfigTmpCompilation($tabConfig)
        );
    }



    /**
     * @inheritdoc
     */
    protected function getStrCompileRenderFormatBefore(
        $strRender,
        array $tabConfig = null
    )
    {
        // Init var
        $result = $this->getStrCompileRenderFormatTmpExtension($strRender);
        $result = $this->getStrCompileRenderFormatTmpInclusion($result);
        $result = $this->getStrCompileRenderFormatTmpCompilation($result, $tabConfig);
        $result = parent::getStrCompileRenderFormatBefore($result, $tabConfig);

        // Return result
        return $result;
    }



}