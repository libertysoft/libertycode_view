<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\php\standard\library;

use liberty_code\view\compiler\format\library\ConstFormat;



class ConstStandardPhpCompiler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_TEMPLATE_REPO = 'objTemplateRepo';



    // Formatting configuration template extension
    const FORMAT_CONFIG_TMP_EXT_TEMPLATE_EXT_REGEXP = array(
        '#@extend\(\s*"([^"]+)"\s*\)#'
    );
    const FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DCL_REGEXP = array(
        '#@setDependency\(\s*"([^"]+)"\s*\)#',
        '#@setDependency\(\s*"([^"]+)"\s*,\s*"([^"]*)"\s*\)#',
        //'#@setDependency\(\s*"([^"]+)"\s*\)((?:(?<!@setDependency).)*?)@endSetDependency#s'
    );
    const FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DCL_SPLIT = array(
        [
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_START_REGEXP => '#@setDependency\(\s*"([^"]+)"\s*\)#',
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_END_REGEXP => '#@endSetDependency#'
        ]
    );
    const FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DEF_REGEXP = array(
        '#@dependency\(\s*"([^"]+)"\s*,\s*"([^"]*)"\s*\)#',
        //'#@dependency\(\s*"([^"]+)"\s*\)((?:(?<!@dependency).)*?)@endDependency#s'
    );
    const FORMAT_CONFIG_TMP_EXT_DEPENDENCY_DEF_SPLIT = array(
        [
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_START_REGEXP => '#@dependency\(\s*"([^"]+)"\s*\)#',
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_END_REGEXP => '#@endDependency#s'
        ]
    );
    const FORMAT_CONFIG_TMP_EXT_DEPENDENCY_PARENT_REGEXP = array(
        '#@parent#'
    );



    // Formatting configuration template inclusion
    const FORMAT_CONFIG_TMP_INC_TEMPLATE_INC_REGEXP = array(
        '#@include\(\s*"([^"]+)"\s*\)#'
    );



    // Formatting configuration template compilation
    const FORMAT_CONFIG_TMP_COMP_TEMPLATE_COMP_REGEXP = array(
        '#@compile\(\s*"([^"]+)"\s*\)#'
    );



    // Exception message constants
    const EXCEPT_MSG_TEMPLATE_REPO_INVALID_FORMAT = 'Following template repository "%1$s" invalid! It must be a template repository object.';



}