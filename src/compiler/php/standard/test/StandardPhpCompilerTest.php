<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/template/repository/test/TmpRepositoryTest.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\view\compiler\format\model\FormatData;
use liberty_code\view\compiler\php\standard\model\StandardPhpCompiler;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init format data
$objFormatDataBefore = new FormatData();
$objFormatDataAfter = new FormatData();

// Init standard PHP compiler
$objStdPhpCompiler = new StandardPhpCompiler(
    $objRegisterTmpRepo,
    array(
        'cache_key_pattern' => 'rnd-saved-%1$s'
    ),
    $objCacheRepo,
    $objFormatDataBefore,
    $objFormatDataAfter
);


