<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/compiler/php/standard/test/StandardPhpCompilerTest.php');



// Init template repositories
$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page',
    '<!doctype html>
<html lang="en">
    <head>
        <title>@setDependency("title")</title>
    </head>
    <body>
        @setDependency("body")
    </body>
</html>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page-content',
    '@extend(  "page"  )
@dependency(  "body"  )
    <div id="content">
        @setDependency(  "content"  )
    </div>
    <footer>
        @setDependency(  "footer"  ,  "@parent @Footer"  )
    </footer>
@endDependency'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-component-msg-content-1',
    '<p>Component: Hello <?= $firstNm; ?> - <?= $nm; ?></p>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-component-msg-layout',
    '<div>
    <h2>Message</h2>
    <p>@setDependency("msg-content")</p>
</div>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-component-msg-1',
    '@extend("component-msg-layout")

@dependency("msg-content")
    @include("component-msg-content-1")
@endDependency'
);



// Test get compiled render
$tabRender = array(
    [
        '@extend("page-content")
@dependency("title", "<?= $title; ?>")

@dependency("content")
    <p>Hello <?= $firstNm; ?> - <?= $nm; ?></p>
@endDependency

@dependency("footer")
    @parent 1
@endDependency',
        [
            'title' => 'Welcome',
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok: template extension formatting
    [
        '@extend("page-content")
@dependency("title", "<?= $title; ?>")

@dependency("content")
    @include("component-msg-content-1")
@endDependency

@dependency("footer")
    @parent 1
@endDependency',
        [
            'title' => 'Welcome',
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok: template extension, template inclusion formatting
    [
        '@extend("page-content")
@dependency("title", "<?= $title; ?>")

@dependency("content")
    @compile("component-msg-1")
@endDependency

@dependency("footer")
    @parent 1
@endDependency',
        [
            'title' => 'Welcome',
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok: template extension, template compilation formatting
);

foreach($tabRender as $render)
{
    $strRender = $render[0];
    $tabArg = $render[1];
    $tabConfig = $render[2];

    echo('Test get compiled render: <br />');
    try{
        echo('Get render: <pre>');var_dump($strRender);echo('</pre>');
        echo('Get arguments: <pre>');var_dump($tabArg);echo('</pre>');
        echo('Get config: <pre>');var_dump($tabConfig);echo('</pre>');

        echo('Get compiled render: <pre>');
        var_dump(htmlentities($objStdPhpCompiler->getStrCompileRender($strRender, $tabArg, $tabConfig)));
        echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


