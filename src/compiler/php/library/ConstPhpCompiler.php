<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\php\library;



class ConstPhpCompiler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const TAB_CONFIG_KEY_PHP_RENDER_PATTERN = 'php_render_pattern';

    // Configuration compile
    const TAB_COMPILE_CONFIG_KEY_PHP_RENDER_PATTERN = 'php_render_pattern';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the PHP compiler configuration standard.';
    const EXCEPT_MSG_COMPILE_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the PHP compiler compiling configuration standard.';
    const EXCEPT_MSG_ARG_INVALID_FORMAT =
        'Following array of arguments "%1$s" invalid! 
        It must be an associative array, where each key must a valid alphanumeric string, not empty.';



}