<?php
/**
 * Description :
 * This class allows to define PHP compiler class.
 * PHP compiler is default compiler, using PHP compilation, to get specific compiled render.
 * Can be consider is base of all PHP compiler types.
 *
 * PHP compiler uses the following specified configuration:
 * [
 *     Default compiler configuration,
 *
 *     php_render_pattern(optional):
 *         "string sprintf pattern,
 *         to build compiled render, where '%1$s' replaced by specified PHP formatted render"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\php\model;

use liberty_code\view\compiler\model\DefaultCompiler;

use liberty_code\view\compiler\library\ConstCompiler;
use liberty_code\view\compiler\php\library\ConstPhpCompiler;
use liberty_code\view\compiler\php\exception\ConfigInvalidFormatException;
use liberty_code\view\compiler\php\exception\CompileConfigInvalidFormatException;
use liberty_code\view\compiler\php\exception\ArgInvalidFormatException;



class PhpCompiler extends DefaultCompiler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Validation
        try
        {
            switch($key)
            {
                case ConstCompiler::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified string PHP formatted render,
     * before compiling.
     *
     * Configuration array format:
     * @see getStrCompileRenderBase() configuration array format.
     *
     * @param string $strRender
     * @param array $tabConfig = null
     * @return string
     * @throws CompileConfigInvalidFormatException
     */
    protected function getStrCompileRenderFormatPhp(
        $strRender,
        array $tabConfig = null
    )
    {
        // Set check argument
        CompileConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabCompileConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstPhpCompiler::TAB_CONFIG_KEY_PHP_RENDER_PATTERN]) ?
                sprintf($tabConfig[ConstPhpCompiler::TAB_CONFIG_KEY_PHP_RENDER_PATTERN], $strRender) :
                (
                    isset($tabCompileConfig[ConstPhpCompiler::TAB_COMPILE_CONFIG_KEY_PHP_RENDER_PATTERN]) ?
                        sprintf($tabCompileConfig[ConstPhpCompiler::TAB_COMPILE_CONFIG_KEY_PHP_RENDER_PATTERN], $strRender) :
                        $strRender
                )
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * @see getStrCompileRenderBase() configuration array format.
     *
     * @inheritdoc
     */
    protected function getStrCompileRenderFormatBefore(
        $strRender,
        array $tabConfig = null
    )
    {
        // Init var
        $result = parent::getStrCompileRenderFormatBefore($strRender, $tabConfig);
        $result = $this->getStrCompileRenderFormatPhp($result, $tabConfig);

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     @see getStrCompileRender() configuration array format,
     *
     *     php_render_pattern(optional):
     *         "string sprintf pattern,
     *         to build compiled render, where '%1$s' replaced by specified PHP compiled render"
     * ]
     *
     * @inheritdoc
     * @throws ArgInvalidFormatException
     */
    protected function getStrCompileRenderBase(
        $strRender,
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Set check argument
        ArgInvalidFormatException::setCheck($tabArg);

        // Set arguments
        foreach($tabArg as $strKey => $arg)
        {
            ${$strKey} = $arg;
        }

        // Evaluate PHP render
        ob_start();
        eval(' ?>' . $strRender . '<?php ');
        $result = ob_get_clean();

        // Return result
        return $result;
    }



}