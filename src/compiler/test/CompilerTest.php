<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\view\compiler\format\model\FormatData;
use liberty_code\view\compiler\php\model\PhpCompiler;



// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init format data
$objFormatDataBefore = new FormatData();
$objFormatDataAfter = new FormatData();

// Init PHP compiler
$objPhpCompiler = new PhpCompiler(
    array(
        'cache_key_pattern' => 'rnd-saved-%1$s'
    ),
    $objCacheRepo,
    $objFormatDataBefore,
    $objFormatDataAfter
);


