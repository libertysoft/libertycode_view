<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/compiler/test/CompilerTest.php');



// Init var
$tabDataSrc = array(
    '#\{([^\}]+)\}#' => function ($strValue, $tabSubValue) {
        return sprintf(
            '<?= %1$s; ?>',
            $tabSubValue[0]
        );
    },
    '#%%([^%%]+)%%#' => function ($strValue, $tabSubValue) {
        return sprintf(
            '<?php //%1$s ?>',
            $tabSubValue[0]
        );
    }
);
$objFormatDataBefore->setDataSrc($tabDataSrc);



// Test config property
echo('Test PHP compiler config: <br />');

try{
    $objPhpCompiler->setTabConfig(array('cache_key_pattern' => 7));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test PHP compiler config: <pre>');var_dump($objPhpCompiler->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test get compiled render
$tabRender = array(
    [
        7,
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ko: bad render format
    [
        '<p>Hello {$firstNm} - {$nm}</p>',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        [
            //'format_before_require' => false
        ]
    ], // Ok
    [
        '<p>Hello word</p>',
        [
            'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ko: bad array of arguments format
    [
        '<p>Hello word</p>',
        [
            'firstNm' => 'John',
            'nm' => 'DOE'
        ],
        null
    ], // Ok
    [
        '<p>Hello-2 <?= $firstNm; ?> - <?= $nm; ?></p>',
        [
            'firstNm' => 'John-2',
            'nm' => 'DOE-2'
        ],
        [
            'cache_require' => 'test'
        ]
    ], // Ko: bad compiling configuration format
    [
        '<p>Hello-2 <?= $firstNm; ?> - <?= $nm; ?></p>',
        [
            'firstNm' => 'John-2',
            'nm' => 'DOE-2'
        ],
        [
            'cache_require' => false,
            'php_render_pattern' => '<?php echo(\'[\'); ?>%1$s<?php echo(\']\') ?>'
        ]
    ] // Ok
);

foreach($tabRender as $render)
{
    $strRender = $render[0];
    $tabArg = $render[1];
    $tabConfig = $render[2];

    echo('Test get compiled render: <br />');
    try{
        echo('Get render: <pre>');var_dump($strRender);echo('</pre>');
        echo('Get arguments: <pre>');var_dump($tabArg);echo('</pre>');
        echo('Get config: <pre>');var_dump($tabConfig);echo('</pre>');

        echo('Get compiled render: <pre>');
        var_dump($objPhpCompiler->getStrCompileRender($strRender, $tabArg, $tabConfig));
        echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test cache
echo('Test cache: <br />');
echo('<pre>');print_r($objCacheRepo->getTabItem($objCacheRepo->getTabSearchKey()));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


