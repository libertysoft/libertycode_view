<?php
/**
 * Description :
 * This class allows to define default compiler class.
 * Can be consider is base of all compiler types.
 *
 * Default compiler uses the following specified configuration:
 * [
 *     cache_require(optional: got true if not found): true / false,
 *
 *     cache_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on cache repository, where '%1$s' replaced by specified render hash",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ],
 *
 *     format_before_require(optional: got true if not found):
 *         true / false,
 *         specify if formatting render, before compiling, required,
 *
 *     format_after_require(optional: got true if not found):
 *         true / false,
 *         specify if formatting render, after compiling, required
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\view\compiler\api\CompilerInterface;

use liberty_code\library\crypto\library\ToolBoxHash;
use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\compiler\format\model\FormatData;
use liberty_code\view\compiler\library\ConstCompiler;
use liberty_code\view\compiler\exception\ConfigInvalidFormatException;
use liberty_code\view\compiler\exception\CacheRepoInvalidFormatException;
use liberty_code\view\compiler\exception\FormatDataInvalidFormatException;
use liberty_code\view\compiler\exception\CompileConfigInvalidFormatException;
use liberty_code\view\compiler\exception\RenderInvalidFormatException;



/**
 * @method array getTabConfig() Get configuration array.
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method null|FormatData getObjFormatDataBefore() Get format data object, used to format render before compiling.
 * @method null|FormatData getObjFormatDataAfter() Get format data object, used to format render after compiling.
 * @method void setTabConfig(array $tabConfig) Set configuration array.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 * @method void setObjFormatDataBefore(null|FormatData $objFormatDataBefore) Set format data object, used to format render before compiling. Format data format: @see getStrCompileRenderFormat() format data key|value formats.
 * @method void setObjFormatDataAfter(null|FormatData $objFormatDataAfter) Set format data object, used to format render after compiling. Format data format: @see getStrCompileRenderFormat() format data key|value formats.
 */
abstract class DefaultCompiler extends FixBean implements CompilerInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     * @param FormatData $objFormatDataBefore = null
     * @param FormatData $objFormatDataAfter = null
     */
	public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null,
        FormatData $objFormatDataBefore = null,
        FormatData $objFormatDataAfter = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }

        // Init format data before, if required
        if(!is_null($objFormatDataBefore))
        {
            $this->setObjFormatDataBefore($objFormatDataBefore);
        }

        // Init format data before, if required
        if(!is_null($objFormatDataAfter))
        {
            $this->setObjFormatDataAfter($objFormatDataAfter);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCompiler::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstCompiler::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstCompiler::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstCompiler::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }

        if(!$this->beanExists(ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_BEFORE))
        {
            $this->__beanTabData[ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_BEFORE] = null;
        }

        if(!$this->beanExists(ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_AFTER))
        {
            $this->__beanTabData[ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_AFTER] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCompiler::DATA_KEY_DEFAULT_CONFIG,
            ConstCompiler::DATA_KEY_DEFAULT_CACHE_REPO,
            ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_BEFORE,
            ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_AFTER
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCompiler::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstCompiler::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;

                case ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_BEFORE:
                case ConstCompiler::DATA_KEY_DEFAULT_FORMAT_DATA_AFTER:
                    FormatDataInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     * @throws CompileConfigInvalidFormatException
     */
    public function checkCacheRequired(array $tabConfig = null)
    {
        // Set check argument
        CompileConfigInvalidFormatException::setCheck($tabConfig);

        // Init var
        $tabCompileConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabCompileConfig[ConstCompiler::TAB_COMPILE_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabCompileConfig[ConstCompiler::TAB_COMPILE_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting before compiling required.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkFormatBeforeRequired(array $tabConfig = null)
    {
        // Init var
        $tabCompileConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjFormatDataBefore())) &&
            (
                (!isset($tabConfig[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE])) ||
                (intval($tabConfig[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabCompileConfig[ConstCompiler::TAB_COMPILE_CONFIG_KEY_FORMAT_BEFORE_REQUIRE])) ||
                (intval($tabCompileConfig[ConstCompiler::TAB_COMPILE_CONFIG_KEY_FORMAT_BEFORE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check formatting after compiling required.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param array $tabConfig = null
     * @return boolean
     */
    protected function checkFormatAfterRequired(array $tabConfig = null)
    {
        // Init var
        $tabCompileConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjFormatDataAfter())) &&
            (
                (!isset($tabConfig[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE])) ||
                (intval($tabConfig[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE]) != 0)
            ) &&
            (
                (!isset($tabCompileConfig[ConstCompiler::TAB_COMPILE_CONFIG_KEY_FORMAT_AFTER_REQUIRE])) ||
                (intval($tabCompileConfig[ConstCompiler::TAB_COMPILE_CONFIG_KEY_FORMAT_AFTER_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string hash,
     * from specified render.
     *
     * @param string $strRender
     * @param array $tabData = array()
     * @return string
     */
    protected function getStrHash(
        $strRender,
        array $tabData = array()
    )
    {
        // Return result
        return
            ToolBoxHash::getStrHash($strRender) .
            ToolBoxHash::getStrHash($tabData, true);
    }



    /**
     * Get specified cache key,
     * from specified render.
     *
     * @param string $strRender
     * @param array $tabData = array()
     * @return string
     */
    protected function getStrCacheKey(
        $strRender,
        array $tabData = array()
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = $this->getStrHash($strRender, $tabData);
        $result = (
            array_key_exists(ConstCompiler::TAB_CONFIG_KEY_CACHE_KEY_PATTERN, $tabConfig) ?
                sprintf($tabConfig[ConstCompiler::TAB_CONFIG_KEY_CACHE_KEY_PATTERN], $result) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get specified string formatted render,
     * from specified format data object.
     *
     * Format data key format:
     * string REGEXP pattern, to found matched values to replace.
     *
     * Format data value (callable) format:
     * string function(string $strValue, array $tabSubValue):
     * Allows to get formatted value to replace,
     * from specified matched value
     * and specified index array of matched (captured parenthesized) sub-values.
     *
     * @param FormatData $objFormatData
     * @param string $strRender
     * @return string
     */
    protected function getStrCompileRenderFormat(
        FormatData $objFormatData,
        $strRender
    )
    {
        // Init var
        $result = $strRender;
        $tabFormatKey = array_keys($objFormatData->getDataSrc());

        // Run each format callable
        foreach($tabFormatKey as $strFormatKey)
        {
            // Get format info
            $strPattern = $strFormatKey;
            $callableFormat = $objFormatData->getValue($strFormatKey);

            // Format render
            $result = preg_replace_callback(
                $strPattern,
                function ($tabMatch) use ($callableFormat)
                {
                    $tabSubValue = $tabMatch;
                    $strValue = array_shift($tabSubValue);
                    return $callableFormat($strValue, $tabSubValue);
                },
                $result
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get specified string formatted render,
     * before compiling.
     * Overwrite it to implement specific feature.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param string $strRender
     * @param array $tabConfig = null
     * @return string
     */
    protected function getStrCompileRenderFormatBefore(
        $strRender,
        array $tabConfig = null
    )
    {
        // Init var
        $result = (
            $this->checkFormatBeforeRequired($tabConfig) ?
                $this->getStrCompileRenderFormat(
                    $this->getObjFormatDataBefore(),
                    $strRender
                ) :
                $strRender
        );

        // Return result
        return $result;
    }



    /**
     * Get specified string formatted render,
     * after compiling.
     * Overwrite it to implement specific feature.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param string $strRender
     * @param array $tabConfig = null
     * @return string
     */
    protected function getStrCompileRenderFormatAfter(
        $strRender,
        array $tabConfig = null
    )
    {
        // Init var
        $result = (
            $this->checkFormatAfterRequired($tabConfig) ?
                $this->getStrCompileRenderFormat(
                    $this->getObjFormatDataAfter(),
                    $strRender
                ) :
                $strRender
        );

        // Return result
        return $result;
    }



    /**
     * Get specified string compiled render basic feature.
     *
     * Arguments array format:
     * @see getStrCompileRender() arguments array format.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param string $strRender
     * @param array $tabArg = array()
     * @param array $tabConfig = null
     * @return string
     */
    abstract protected function getStrCompileRenderBase(
        $strRender,
        array $tabArg = array(),
        array $tabConfig = null
    );



    /**
     * Get specified string compiled render engine.
     *
     * Arguments array format:
     * @see getStrCompileRender() arguments array format.
     *
     * Configuration array format:
     * @see getStrCompileRender() configuration array format.
     *
     * @param string $strRender
     * @param array $tabArg = array()
     * @param array $tabConfig = null
     * @return string
     */
    protected function getStrCompileRenderEngine(
        $strRender,
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Init var
        $result = $this->getStrCompileRenderFormatBefore(
            $strRender,
            $tabConfig
        );
        $result = $this->getStrCompileRenderBase(
            $result,
            $tabArg,
            $tabConfig
        );
        $result = $this->getStrCompileRenderFormatAfter(
            $result,
            $tabConfig
        );

        // Return result
        return $result;
    }



    /**
     * Configuration array format:
     * [
     *     cache_require(optional: got true if not found): true / false,
     *
     *     format_before_require(optional: got true if not found):
     *         true / false,
     *         specify if formatting render, before compiling, required,
     *
     *     format_after_require(optional: got true if not found):
     *         true / false,
     *         specify if formatting render, after compiling, required
     * ]
     *
     * @inheritdoc
     * @throws RenderInvalidFormatException
     */
    public function getStrCompileRender(
        $strRender,
        array $tabArg = array(),
        array $tabConfig = null
    )
    {
        // Set check argument
        RenderInvalidFormatException::setCheck($strRender);

        // Init var
        $tabCompileConfig = $tabConfig;
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired($tabCompileConfig) ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheKey($strRender, $tabArg),
                    function() use ($strRender, $tabArg, $tabCompileConfig) {
                        return $this->getStrCompileRenderEngine(
                            $strRender,
                            $tabArg,
                            $tabCompileConfig
                        );
                    },
                    (
                        isset($tabConfig[ConstCompiler::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstCompiler::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getStrCompileRenderEngine(
                    $strRender,
                    $tabArg,
                    $tabCompileConfig
                )
        );

        // Return result
        return $result;
    }



}