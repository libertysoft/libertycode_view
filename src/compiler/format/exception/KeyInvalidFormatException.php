<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\format\exception;

use Exception;

use liberty_code\view\compiler\format\library\ConstFormatData;



class KeyInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $key
     */
	public function __construct($key)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstFormatData::EXCEPT_MSG_KEY_INVALID_FORMAT, strval($key));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified key has valid format.
	 *
     * @param mixed $key
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($key)
    {
		// Init var
        $result = (
            is_string($key) &&
            (trim($key) !== '')
        );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($key);
		}
		
		// Return result
		return $result;
    }
	
	
	
}