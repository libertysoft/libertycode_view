<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\view\compiler\format\model\FormatData;



// Init var
$tabDataSrc = array(
	'#\{([^\}]+)\}#' => function ($strValue, $tabSubValue) {
	    return sprintf(
	        '<?= %1$s; ?>',
            $tabSubValue[0]
        );
    },
    '#\[([^\]]+)\]#' => function ($strValue, $tabSubValue) {
        return sprintf(
            '<?= %1$s; ?>',
            $tabSubValue[0]
        );
    }
);

$objData = new FormatData();



// Test properties
echo('Test properties : <br />');

try{
	$objData->setDataSrc(array(
		'test-1' => 'Test 1',
		'test-2' => 1
	));
} catch(\Exception $e) {
	echo($e->getMessage());
	echo('<br />');
}

$objData->setDataSrc($tabDataSrc);

echo('Source data: <pre>');print_r($objData->getDataSrc());echo('</pre>');

echo('<br /><br /><br />');



// Test put
$tabKey = array(
    '#\{([^\}]+)\}#' => 'Value test', // Ko: bad value format

    '#%%([^%%]+)%%#' => function ($strValue, $tabSubValue) {
        return sprintf(
            '<?php //%1$s ?>',
            $tabSubValue[0]
        );
    }, // Ok: create

    function ($strValue, $tabSubValue) {
        return sprintf(
            '<?php echo(%1$s); ?>',
            $tabSubValue[0]
        );
    }, // Ko: bad key format

    '#\[([^\]]+)\]#' => function ($strValue, $tabSubValue) {
        return sprintf(
            '<?php echo(%1$s); ?>',
            $tabSubValue[0]
        );
    }, // Ok: update

    'test' => 7.7 // Ko: bad value format
);

foreach($tabKey as $strKey => $value)
{
	echo('Test put "'.$strKey.'": <br />');
	try{
		echo('<pre>');var_dump($objData->putValue($strKey, $value));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test putting: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');



// Test check, get
$tabKey = array(
    '#\{([^\}]+)\}#' => 'Test {value 1} - {value 2}', // Ok

    'test' => 'Test {value 1} - {value 2}', // Ko: not found

    '#\[([^\]]+)\]#' => 'Test', // Ok: no replacement

    '#%%([^%%]+)%%#' => "Test\n
    %% Comment 1 %%\n
    T1\n
    %% Comment 2 %%\n
    T2\n" // Ok
);

foreach($tabKey as $strKey => $value)
{
    echo('Test check, get "'.$strKey.'": <br />');
    try{
        $callable = $objData->getValue($strKey);

        if($objData->checkValueExists($strKey))
        {
            echo('<pre>');
            var_dump(preg_replace_callback(
                $strKey,
                function ($tabMatch) use ($callable)
                {
                    $tabSubValue = $tabMatch;
                    $strValue = array_shift($tabSubValue);
                    return $callable($strValue, $tabSubValue);
                },
                $value
            ));
            echo('</pre>');
        }
        else
        {
            echo('Key not found<br />');
        }
    } catch(\Exception $e) {
        echo(htmlentities($e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test remove
$tabKey = array(
	'test', // Ko: not found
    '#\[([^\]]+)\]#' // Ok
);

foreach($tabKey as $strKey)
{
	echo('Test remove "'.$strKey.'": <br />');
	try{
		echo('<pre>');var_dump($objData->removeValue($strKey));echo('</pre>');
	} catch(\Exception $e) {
		echo(htmlentities($e->getMessage()));
		echo('<br />');
	}
	echo('<br />');
}

echo('Test removing: <br />');
echo('<pre>');print_r($objData->getDataSrc());echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


