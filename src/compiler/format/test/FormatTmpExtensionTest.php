<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/template/repository/test/TmpRepositoryTest.php');

// Use
use liberty_code\view\compiler\format\library\ToolBoxFormatTmpExtension;



// Init var
$tabConfig = array(
    'template_extend_regexp' => [
        '#<extend key="([^"]+)"/>#'
    ],
    'dependency_declaration_regexp' => [
        '#<set-dependency key="([^"]+)"/>#',
        '#<set-dependency key="([^"]+)" value="([^"]+)"/>#',
        //'#<set-dependency key="([^"]+)">(.+?)</set-dependency>#s'
    ],
    'dependency_declaration_split' => [
        [
            'start_regexp' => '#<set-dependency key="([^"]+)">#',
            'end_regexp' => '#</set-dependency>#'
        ]
    ],
    'dependency_definition_regexp' => [
        '#<dependency key="([^"]+)" value="([^"]+)"/>#',
        //'#<dependency key="([^"]+)">(.+?)</dependency>#s'
    ],
    'dependency_definition_split' => [
        [
            'start_regexp' => '#<dependency key="([^"]+)">#',
            'end_regexp' => '#</dependency>#'
        ]
    ],
    'dependency_parent_regexp' => [
        '#<parent/>#'
    ]
);

// Init template repositories
$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page',
    '<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><set-dependency key="title"/></title>
    </head>
    <body>
        <set-dependency key="body"/>
    </body>
</html>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page-content',
    '<extend key="page"/>
<dependency key="body">
    <div id="content">
        <set-dependency key="content">
            <parent/>
            <p>No content found</p>
        </set-dependency>
    </div>
    <footer>
        <set-dependency key="footer" value="Footer"/>
    </footer>
</dependency>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page-content-1',
    '<extend key="page-content"/>

<dependency key="title" value="Content 1"/>

<dependency key="content">
    <p>Content 1 text ...</p>
</dependency>

<dependency key="footer">
    <strong><parent/></strong>
    @<parent/> 1
</dependency>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page-content-2',
    '<extend key="page-content"/>

<dependency key="title" value="Content 2"/>

<dependency key="content">
    <p>Content 2 text ...</p>
</dependency>

<dependency key="footer">
    @<parent/> 2
</dependency>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page-content-3',
    '<extend key="page-content-2"/>

<dependency key="title" value="Content 3"/>

<dependency key="content">
    <p>Content 3 text ...</p>
</dependency>

<dependency key="test1">
    <p>Test-1 3 text ...</p>
</dependency>

<dependency key="test2" value="Test-2 3"/>'
);



// Test get formatted render
$tabKey = array(
    'page', // Ok
    'page-content', // Ok
    'page-content-1', // Ok
    'page-content-2', // Ok
    'page-content-3' // Ok: overwrite title, content from page-content-2
);

foreach($tabKey as $strKey)
{
    echo('Test get formatted render, from template content "'.$strKey.'": <br />');
    try{
        $strRender = $objRegisterTmpRepo->getStrContent($strKey);
        $tabInfo = array();
        $strRenderFormat = ToolBoxFormatTmpExtension::getStrRenderFormat(
            $strRender,
            $objRegisterTmpRepo,
            $tabConfig,
            $tabInfo
        );
        echo('Get formatted render: <pre>');print_r(htmlentities($strRenderFormat));echo('</pre>');
        echo('Get info: <pre>');var_dump($tabInfo);echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


