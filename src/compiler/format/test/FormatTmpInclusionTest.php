<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/template/repository/test/TmpRepositoryTest.php');

// Use
use liberty_code\view\compiler\format\library\ToolBoxFormatTmpInclusion;



// Init var
$tabConfig = array(
    'template_include_regexp' => [
        '#<include key="([^"]+)"/>#'
    ],
    //*
    'template_content_format_callable' =>
        function($strKey, $strContent, $objRepository, array $tabConfig, array &$tabInfo) {
            $tabInfo['format'] = (
                (
                    array_key_exists('format', $tabInfo) &&
                    is_array($tabInfo['format'])
                ) ?
                    $tabInfo['format'] :
                    array()
            );
            if(!in_array($strKey, $tabInfo['format']))
            {
                $tabInfo['format'][] = $strKey;
            }

            return sprintf('format[%1$s]', $strContent);
        }
    //*/
);

// Init template repositories
$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-page',
    '<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><set-dependency key="title"/></title>
    </head>
    <body>
        <include key="component-1"/>
        <br />
        <include key="component-2"/>
        <br />
        <include key="component-3"/>
    </body>
</html>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-component-1',
    '<div>
        <p>Comp 1</p>
    </div>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-component-2',
    '<div>
        <p>Comp 2</p>
    </div>'
);

$objRegisterTmpRepo->getObjRegister()->putItem(
    'tmp-component-3',
    '<div>
        <p>Comp 3</p>
    </div>'
);



// Test get formatted render
$tabKey = array(
    'component-1', // Ok
    'component-2', // Ok
    'component-3', // Ok
    'page' // Ok
);

foreach($tabKey as $strKey)
{
    echo('Test get formatted render, from template content "'.$strKey.'": <br />');
    try{
        $strRender = $objRegisterTmpRepo->getStrContent($strKey);
        $tabInfo = array();
        $strRenderFormat = ToolBoxFormatTmpInclusion::getStrRenderFormat(
            $strRender,
            $objRegisterTmpRepo,
            $tabConfig,
            $tabInfo
        );
        echo('Get formatted render: <pre>');print_r(htmlentities($strRenderFormat));echo('</pre>');
        echo('Get info: <pre>');var_dump($tabInfo);echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');


