<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\format\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\format\library\ConstFormat;



class ToolBoxFormatTmpExtension extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of string REGEXP patterns,
     * from specified configuration.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * @param array $tabConfig
     * @param string $strKey
     * @param array|mixed $default = array()
     * @return array
     */
    protected static function getTabConfigRegexp(
        array $tabConfig,
        $strKey,
        $default = array()
    )
    {
        // Return result
        return (
            (
                isset($tabConfig[$strKey]) &&
                is_array($tabConfig[$strKey])
            ) ?
                array_values($tabConfig[$strKey]) :
                $default
        );
    }



    /**
     * Get dependency split configuration array,
     * from specified configuration.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * @param array $tabConfig
     * @param string $strKey
     * @param array|mixed $default = array()
     * @return array|mixed
     */
    protected static function getTabDependencySplitConfig(
        array $tabConfig,
        $strKey,
        $default = array()
    )
    {
        // Return result
        return (
            (
                isset($tabConfig[$strKey]) &&
                is_array($tabConfig[$strKey])
            ) ?
                array_values($tabConfig[$strKey]) :
                $default
        );
    }



    /**
     * Get string parent template key,
     * from specified render.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * Information array format:
     * @see getStrRenderFormat() information array format.
     *
     * @param string $strRender
     * @param array $tabConfig
     * @param array &$tabInfo = array()
     * @return null|string
     */
    protected static function getStrParentTemplateKey(
        $strRender,
        array $tabConfig,
        array &$tabInfo = array()
    )
    {
        // Init var
        $result = null;
        $tabTemplateExtPattern = static::getTabConfigRegexp(
            $tabConfig,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_TEMPLATE_EXT_REGEXP,
            null
        );

        // Run each template extend patterns, if required
        if(is_string($strRender) && (!is_null($tabTemplateExtPattern)))
        {
            for($intCpt = 0; ($intCpt < count($tabTemplateExtPattern)) && is_null($result); $intCpt++)
            {
                // Get template key, if found
                $tabMatch = array();
                $strPattern = $tabTemplateExtPattern[$intCpt];
                $result = (
                    (
                        is_string($strPattern) &&
                        (preg_match($strPattern, $strRender, $tabMatch) !== false) &&
                        isset($tabMatch[1])
                    ) ?
                        $tabMatch[1] :
                        $result
                );
            }
        }

        // Set info, if required
        if(
            (!is_null($result)) &&
            (!in_array($result, $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_PARENT_TEMPLATE_KEY]))
        )
        {
            $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_PARENT_TEMPLATE_KEY][] = $result;
        }

        // Return result
        return $result;
    }



    /**
     * Get array of dependencies,
     * from specified render,
     * and specified dependency split configuration.
     *
     * Dependency split configuration array format:
     * [
     *     // Split 1
     *     [
     *         start_regexp => 'string REGEXP pattern:
     *             allowing to match with specific dependency start expression,
     *             where first captured parenthesized value is the dependency key,
     *             and considered as dependency value, all characters after its expression.',
     *
     *         end_regexp => 'string REGEXP pattern:
     *             allowing to match with specific dependency end expression,
     *             where considered as dependency value, all characters before its expression.'
     *     ],
     *
     *     ...,
     *
     *     // Split N
     *     [...]
     * ]
     *
     * Return format:
     * [
     *     // Dependency 1
     *     'string dependency 1 key' => [
     *         'string dependency 1 start expression',
     *         'string dependency 1 end expression',
     *         null or 'string dependency 1 value',
     *     ]
     *     ...,
     *     'string dependency N key' => [...]
     * ]
     *
     * @param string $strRender
     * @param array $tabDependencySplitConfig
     * @return array
     */
    protected static function getTabDependencyFromSplit(
        $strRender,
        array $tabDependencySplitConfig = array()
    )
    {
        // Init var
        $result = array();

        if(is_string($strRender))
        {
            // Run each dependency split configuration
            foreach($tabDependencySplitConfig as $dependencySplitConfig)
            {
                // Get dependency start pattern, dependency end pattern
                $strStartPattern = (
                    (
                        isset($dependencySplitConfig[ConstFormat::TAB_TMP_EXT_CONFIG_KEY_START_REGEXP]) &&
                        is_string($dependencySplitConfig[ConstFormat::TAB_TMP_EXT_CONFIG_KEY_START_REGEXP])
                    ) ?
                        $dependencySplitConfig[ConstFormat::TAB_TMP_EXT_CONFIG_KEY_START_REGEXP] :
                        null
                );
                $strEndPattern = (
                    (
                        isset($dependencySplitConfig[ConstFormat::TAB_TMP_EXT_CONFIG_KEY_END_REGEXP]) &&
                        is_string($dependencySplitConfig[ConstFormat::TAB_TMP_EXT_CONFIG_KEY_END_REGEXP])
                    ) ?
                        $dependencySplitConfig[ConstFormat::TAB_TMP_EXT_CONFIG_KEY_END_REGEXP] :
                        null
                );

                // Check dependency start pattern, dependency end pattern valid match
                $tabStartMatch = array();
                $tabEndMatch = array();
                if(
                    (!is_null($strStartPattern)) &&
                    (!is_null($strEndPattern)) &&
                    (preg_match_all($strStartPattern, $strRender, $tabStartMatch, PREG_SET_ORDER) > 0) &&
                    (preg_match_all($strEndPattern, $strRender, $tabEndMatch, PREG_SET_ORDER) > 0)
                )
                {
                    // Get info
                    $tabStart = array();
                    $tabKey = array();
                    foreach($tabStartMatch as $startMatch)
                    {
                        if(isset($startMatch[1]))
                        {
                            $tabStart[] = $startMatch[0];
                            $tabKey[] = $startMatch[1];
                        }
                    }

                    // Run each dependency start
                    for($intCpt = 0; $intCpt < count($tabStart); $intCpt++)
                    {
                        // Get dependency key
                        $strStart = $tabStart[$intCpt];
                        $strKey = $tabKey[$intCpt];

                        // Get dependency value, if required (run each dependency end)
                        $strPartRender = explode($strStart, $strRender)[1];
                        $strEnd = null;
                        $strValue = null;
                        for($intCpt2 = 0; ($intCpt2 < count($tabEndMatch)) && is_null($strValue); $intCpt2++)
                        {
                            // Register dependency value, if required: dependency end found, no dependency start found
                            $endMatch = $tabEndMatch[$intCpt2];
                            $strEnd = $endMatch[0];
                            if(strrpos($strPartRender, $strEnd) !== false)
                            {
                                $strPartRender = explode($strEnd, $strPartRender)[0];
                                $strValue = (
                                (
                                    count(array_filter(
                                        $tabStart,
                                        function($strStart) use ($strPartRender) {
                                            return (strrpos($strPartRender, $strStart) !== false);
                                        }
                                    )) == 0) ?
                                        $strPartRender :
                                        $strValue
                                );
                            }
                        }

                        // Register dependency, if required
                        if(!is_null($strValue))
                        {
                            $result[$strKey] = array(
                                $strStart,
                                $strEnd,
                                $strValue
                            );
                        }
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get array of dependencies,
     * from specified render.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * Dependencies array format:
     * Return format:
     * @see getStrRender() dependencies array format.
     *
     * Information array format:
     * @see getStrRenderFormat() information array format.
     *
     * @param string $strRender
     * @param array $tabConfig
     * @param array $tabDependency = array()
     * @param array &$tabInfo = array()
     * @return array
     */
    protected static function getTabDependency(
        $strRender,
        array $tabConfig,
        array $tabDependency = array(),
        array &$tabInfo = array()
    )
    {
        // Init var
        $result = $tabDependency;

        if(is_string($strRender))
        {
            // Get array of dependencies, from dependency declaration patterns
            $tabTmpDependency = array();
            $tabDependencyDclPattern = static::getTabConfigRegexp(
                $tabConfig,
                ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_REGEXP
            );
            foreach($tabDependencyDclPattern as $strPattern)
            {
                // Get dependency keys, if found
                $tabMatch = array();
                if(
                    is_string($strPattern) &&
                    (preg_match_all($strPattern, $strRender, $tabMatch, PREG_SET_ORDER) > 0)
                )
                {
                    // Run each matched expression
                    foreach($tabMatch as $match)
                    {
                        // Register dependency, with dependency value, if required
                        $strKey = (isset($match[1]) ? $match[1] : null);
                        if(!is_null($strKey))
                        {
                            $strValue = (isset($match[2]) ? $match[2] : null);
                            $tabTmpDependency[$strKey] = $strValue;
                        }
                    }
                }
            }

            // Add array of dependencies, from dependency declaration split configuration
            $tabDependencyDclSplitConfig = static::getTabDependencySplitConfig(
                $tabConfig,
                ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_SPLIT
            );
            $tabTmpDependency = array_merge(
                $tabTmpDependency,
                array_map(
                    function(array $tabInfo)
                    {
                        return $tabInfo[2];
                    },
                    static::getTabDependencyFromSplit($strRender, $tabDependencyDclSplitConfig)
                )
            );

            // Register array of dependencies, from dependency declarations
            foreach($tabTmpDependency as $strKey => $strValue)
            {
                // Register dependency
                static::setDependency(
                    $tabConfig,
                    $result,
                    $strKey,
                    $strValue
                );

                // Set info, if required
                if(!in_array($strKey, $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY]))
                {
                    $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY][] = $strKey;
                }

                if(in_array($strKey, $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DCL_NOT_FOUND]))
                {
                    $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DCL_NOT_FOUND] = array_values(array_filter(
                        $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DCL_NOT_FOUND],
                        function($strKeyNotFound) use ($strKey)
                        {
                            return (
                                is_string($strKeyNotFound) &&
                                ($strKeyNotFound != $strKey)
                            );
                        }
                    ));
                }

                if(
                    is_null($result[$strKey]) &&
                    (!in_array($strKey, $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND]))
                )
                {
                    $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND][] = $strKey;
                }
            }

            // Get array of dependencies, from dependency definition patterns
            $tabTmpDependency = array();
            $tabDependencyDefPattern = static::getTabConfigRegexp(
                $tabConfig,
                ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DEF_REGEXP
            );
            foreach($tabDependencyDefPattern as $strPattern)
            {
                // Get dependency keys and values, if required
                $tabMatch = array();
                if(
                    is_string($strPattern) &&
                    (preg_match_all($strPattern, $strRender, $tabMatch, PREG_SET_ORDER) > 0)
                )
                {
                    // Run each matched expression
                    foreach($tabMatch as $match)
                    {
                        // Register dependency, if required
                        $strKey = (isset($match[1]) ? $match[1] : null);
                        $strValue = (isset($match[2]) ? $match[2] : null);
                        if(
                            (!is_null($strKey)) &&
                            (!is_null($strValue))
                        )
                        {
                            $tabTmpDependency[$strKey] = $strValue;
                        }
                    }
                }
            }

            // Add array of dependencies, from dependency definition split configuration
            $tabDependencyDefSplitConfig = static::getTabDependencySplitConfig(
                $tabConfig,
                ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DEF_SPLIT
            );
            $tabTmpDependency = array_merge(
                $tabTmpDependency,
                array_map(
                    function(array $tabInfo)
                    {
                        return $tabInfo[2];
                    },
                    static::getTabDependencyFromSplit($strRender, $tabDependencyDefSplitConfig)
                )
            );

            // Register array of dependencies, from definition definitions
            foreach($tabTmpDependency as $strKey => $strValue)
            {
                // Register dependency
                static::setDependency(
                    $tabConfig,
                    $result,
                    $strKey,
                    $strValue
                );

                // Set info, if required
                if(!in_array($strKey, $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY]))
                {
                    $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY][] = $strKey;
                }

                if(in_array($strKey, $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND]))
                {
                    $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND] = array_values(array_filter(
                        $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND],
                        function($strKeyNotFound) use ($strKey)
                        {
                            return (
                                is_string($strKeyNotFound) &&
                                ($strKeyNotFound != $strKey)
                            );
                        }
                    ));
                }
                else
                {
                    $tabInfo[ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DCL_NOT_FOUND][] = $strKey;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified array of formatted dependencies.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * Dependencies array format:
     * Return array format:
     * @see getStrRender() dependencies array format.
     *
     * @param array $tabConfig
     * @param array $tabDependency
     * @return array
     */
    protected static function getTabDependencyFormat(
        array $tabConfig,
        array $tabDependency
    )
    {
        // Init var
        $tabDependencyParentPattern = static::getTabConfigRegexp(
            $tabConfig,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_PARENT_REGEXP,
            null
        );
        $result = array_map(
            function($strValue) use ($tabDependencyParentPattern)
            {
                return (
                    is_string($strValue) ?
                        preg_replace(
                            $tabDependencyParentPattern,
                            '',
                            $strValue
                        ) :
                        $strValue
                );
            },
            $tabDependency
        );

        // Return result
        return $result;
    }



    /**
     * Get information array.
     *
     * Information array format:
     * Return array format:
     * @see getStrRenderFormat() information array format.
     *
     * @param array $tabInfo = array()
     * @return array
     */
    protected static function getTabInfo(
        array $tabInfo = array()
    )
    {
        // Init var
        $result = array();

        // Hydrate information
        $tabKey = array(
            ConstFormat::TAB_TMP_EXT_INFO_KEY_PARENT_TEMPLATE_KEY,
            ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY,
            ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DCL_NOT_FOUND,
            ConstFormat::TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND
        );
        foreach($tabKey as $strKey)
        {
            $result[$strKey] = (
                (
                    array_key_exists($strKey, $tabInfo) &&
                    is_array($tabInfo[$strKey])
                ) ?
                    $tabInfo[$strKey] :
                    array()
            );
        }

        // Return result
        return $result;
    }



    /**
     * Get string render to return,
     * from specified render.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * Dependencies array format:
     * [
     *     // Dependency 1
     *     'string dependency key 1' => null or 'string dependency value 1',
     *     ...,
     *     'string dependency key N' => ...
     * ]
     *
     * Information format:
     * @see getStrRenderFormat() information array format.
     *
     * @param string $strRender
     * @param TmpRepositoryInterface $objRepository
     * @param array $tabConfig
     * @param array &$tabDependency = array()
     * @param array &$tabInfo = array()
     * @return string
     */
    protected static function getStrRender(
        $strRender,
        TmpRepositoryInterface $objRepository,
        array $tabConfig,
        array &$tabDependency = array(),
        array &$tabInfo = array()
    )
    {
        // Init var
        $strParentTemplateKey = static::getStrParentTemplateKey(
            $strRender,
            $tabConfig,
            $tabInfo
        );
        $tabDependency = static::getTabDependency(
            $strRender,
            $tabConfig,
            $tabDependency,
            $tabInfo
        );
        $result = (
            (!is_null($strParentTemplateKey)) ?
                static::getStrRender(
                    $objRepository->getStrContent($strParentTemplateKey),
                    $objRepository,
                    $tabConfig,
                    $tabDependency,
                    $tabInfo
                ) :
                $strRender
        );

        // Return result
        return $result;
    }



    /**
     * Get specified string formatted render,
     * from specified array of dependencies.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * Dependencies array format:
     * @see getStrRender() dependencies array format.
     *
     * @param string $strRender
     * @param array $tabConfig
     * @param array $tabDependency
     * @return string
     */
    protected static function getStrRenderFormatDependency(
        $strRender,
        array $tabConfig,
        array $tabDependency
    )
    {
        // Init var
        $result = (is_string($strRender) ? $strRender : '');
        $boolFormat = false;
        $tabDependencyDclPattern = static::getTabConfigRegexp(
            $tabConfig,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_REGEXP
        );

        // Run each dependency declaration patterns
        foreach($tabDependencyDclPattern as $strPattern)
        {
            // Replace each dependency declaration matched expression, if required
            $result = (
                is_string($strPattern) ?
                    preg_replace_callback
                    (
                        $strPattern,
                        function (array $tabMatch) use ($tabDependency, &$boolFormat)
                        {
                            // Init var
                            $strKey = (isset($tabMatch[1]) ? $tabMatch[1] : null);

                            // Get replaced value
                            $result = $tabMatch[0];
                            if(!is_null($strKey))
                            {
                                $result = '';
                                if(
                                    array_key_exists($strKey, $tabDependency) &&
                                    is_string($tabDependency[$strKey])
                                )
                                {
                                    $result = $tabDependency[$strKey];
                                    $boolFormat = true;
                                }
                            }

                            // Return result
                            return $result;
                        },
                        $result
                    ) :
                    $result
            );
        }

        // Run each dependency, from dependency declaration split configuration
        $tabDependencyDclSplitConfig = static::getTabDependencySplitConfig(
            $tabConfig,
            ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_SPLIT
        );
        $tabDependencyFromSplit = static::getTabDependencyFromSplit($result, $tabDependencyDclSplitConfig);
        foreach($tabDependencyFromSplit as $strKey => $tabInfo)
        {
            // Get info
            $strStart = $tabInfo[0];
            $strEnd = $tabInfo[1];
            $strValue = $tabInfo[2];

            // Replace dependency declaration expression, if required
            if(
                array_key_exists($strKey, $tabDependency) &&
                is_string($tabDependency[$strKey])
            )
            {
                $result = str_replace(
                    ($strStart . $strValue . $strEnd),
                    $tabDependency[$strKey],
                    $result
                );
                $boolFormat = true;
            }
        }

        // Get re-formatted render, if required
        $result = (
            ($boolFormat) ?
                static::getStrRenderFormatDependency(
                    $result,
                    $tabConfig,
                    $tabDependency
                ) :
                $result
        );

        // Return result
        return $result;
    }



    /**
     * Get specified string formatted render,
     * using template extension formatting.
     *
     * Feature description:
     * - Extension:
     * Specified render can extend parent template.
     * Parent template render will be returned,
     * where each dependency declaration from parent template,
     * will be replaced by dependency definition from specified render.
     * - Recursive extension:
     * Parent template can extend other parent template, etc....
     * First parent template render will be returned,
     * where each dependency declaration, from all parent templates,
     * will be replaced, where it's located, by dependency definition from specified render and all parent templates.
     * To ensure recursion, it's possible to set dependency declarations on dependency definitions.
     * - Dependency definition initialization:
     * It's possible to initialize dependency definition (like default definition),
     * at same time as declaration.
     * - Dependency definition overwriting:
     * If many dependency definitions found for same dependency declaration,
     * the last dependency definition will be returned.
     * Each parent clause, from dependency definition,
     * will be replaced, where it's located, by previous dependency definition.
     *
     * Configuration array format:
     * [
     *     template_extend_regexp => [
     *         Index array of string REGEXP patterns:
     *             - Each REGEXP pattern must allowing to match with specific template extend expression,
     *             where first captured parenthesized value is the template key.
     *             - Only first valid matching REGEXP pattern is used, per template.
     *             - If no valid matching REGEXP pattern found, no extension done.
     *     ],
     *
     *     dependency_declaration_regexp => [
     *         Index array of string REGEXP patterns:
     *             - Each REGEXP pattern must allowing to match with specific dependency declaration expression,
     *             where first captured parenthesized value is the dependency key,
     *             and optional second captured parenthesized value is the dependency value (content to render).
     *     ],
     *
     *     dependency_declaration_split => [
     *         @see getTabDependencyFromSplit() configuration array format
     *     ],
     *
     *     dependency_definition_regexp => [
     *         Index array of string REGEXP patterns:
     *             - Each REGEXP pattern must allowing to match with specific dependency definition expression,
     *             where first captured parenthesized value is the dependency key,
     *             and second captured parenthesized value is the dependency value (content to render).
     *     ],
     *
     *     dependency_definition_split => [
     *         @see getTabDependencyFromSplit() configuration array format
     *     ],
     *
     *     dependency_parent_regexp => [
     *         Index array of string REGEXP patterns:
     *             - Each REGEXP pattern must allowing to match with specific dependency parent expression.
     *     ]
     * ]
     *
     * Information array format:
     * [
     *     parent_template_key => [
     *         'string template key 1',
     *         ...,
     *         'string template key N'
     *     ],
     *
     *     dependency_key => [
     *         'string dependency key 1',
     *         ...,
     *         'string dependency key N'
     *     ],
     *
     *     dependency_key_declaration_not_found => [
     *         'string dependency key 1',
     *         ...,
     *         'string dependency key N'
     *     ],
     *
     *     dependency_key_definition_not_found => [
     *         'string dependency key 1',
     *         ...,
     *         'string dependency key N'
     *     ]
     * ]
     *
     * @param string $strRender
     * @param TmpRepositoryInterface $objRepository
     * @param array $tabConfig
     * @param array &$tabInfo = array()
     * @return string
     */
    public static function getStrRenderFormat(
        $strRender,
        TmpRepositoryInterface $objRepository,
        array $tabConfig,
        array &$tabInfo = array()
    )
    {
        // Init var
        $tabInfo = static::getTabInfo($tabInfo);
        $tabDependency = array();
        $result = static::getStrRender(
            $strRender,
            $objRepository,
            $tabConfig,
            $tabDependency,
            $tabInfo
        );
        $tabDependency = static::getTabDependencyFormat(
            $tabConfig,
            $tabDependency
        );
        $result = static::getStrRenderFormatDependency(
            $result,
            $tabConfig,
            $tabDependency
        );

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set specified dependency value,
     * on specified array of dependencies.
     *
     * Configuration array format:
     * @see getStrRenderFormat() configuration array format.
     *
     * Dependencies array format:
     * @see getStrRender() dependencies array format.
     *
     * @param array $tabConfig
     * @param array &$tabDependency
     * @param string $strKey
     * @param null|string $strValue = null
     */
    protected static function setDependency(
        array $tabConfig,
        array &$tabDependency,
        $strKey,
        $strValue = null
    )
    {
        // Init var
        $strValue = (is_string($strValue) ? $strValue : null);

        // Set value, on array of dependencies, if required
        if(is_string($strKey))
        {
            // Get value
            $tabDependencyParentPattern = static::getTabConfigRegexp(
                $tabConfig,
                ConstFormat::TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_PARENT_REGEXP,
                null
            );
            $strValue = (
                (
                    array_key_exists($strKey, $tabDependency) &&
                    is_string($tabDependency[$strKey])
                ) ?
                    (
                        (
                            is_null($tabDependencyParentPattern) ||
                            is_null($strValue)
                        ) ?
                            $tabDependency[$strKey] :
                            preg_replace(
                                $tabDependencyParentPattern,
                                $strValue,
                                $tabDependency[$strKey]
                            )
                    ) :
                    $strValue
            );

            // Set value, on array of dependencies
            $tabDependency[$strKey] = $strValue;
        }
    }



}