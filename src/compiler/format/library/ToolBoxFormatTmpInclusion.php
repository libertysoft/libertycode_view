<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\format\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\format\library\ConstFormat;



class ToolBoxFormatTmpInclusion extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified string formatted render,
     * using template inclusion formatting.
     *
     * Feature description:
     * Specified render will be returned,
     * where each template inclusion will be replaced by template render.
     *
     * Array of configuration format:
     * [
     *     template_include_regexp => [
     *         Index array of string REGEXP patterns:
     *             - Each REGEXP pattern must allowing to match with specific template include expression,
     *             where first captured parenthesized value is the template key.
     *     ],
     *     template_content_format_callable(optional):
     *         Get formatted template content callback function:
     *         string function(
     *             string $strKey,
     *             string $strContent,
     *             TmpRepositoryInterface $objRepository,
     *             array $tabConfig,
     *             array &$tabInfo
     *         )
     * ]
     *
     * Array of information format:
     * [
     *     include_template_key => [
     *         'string template key 1',
     *         ...,
     *         'string template key N'
     *     ]
     * ]
     *
     * @param string $strRender
     * @param TmpRepositoryInterface $objRepository
     * @param array $tabConfig
     * @param array &$tabInfo = array()
     * @return string
     */
    public static function getStrRenderFormat(
        $strRender,
        TmpRepositoryInterface $objRepository,
        array $tabConfig,
        array &$tabInfo = array()
    )
    {
        // Init var
        $result = '';
        $tabTemplateIncPattern = (
            (
                isset($tabConfig[ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_INC_REGEXP]) &&
                is_array($tabConfig[ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_INC_REGEXP])
            ) ?
                array_values($tabConfig[ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_INC_REGEXP]) :
                array()
        );
        $formatCallable = (
            (
                isset($tabConfig[ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_CONTENT_FORMAT_CALLABLE]) &&
                is_callable($tabConfig[ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_CONTENT_FORMAT_CALLABLE])
            ) ?
                $tabConfig[ConstFormat::TAB_TMP_INC_CONFIG_KEY_TEMPLATE_CONTENT_FORMAT_CALLABLE] :
                null
        );
        $tabInfo = array(
            ConstFormat::TAB_TMP_INC_INFO_KEY_INC_TEMPLATE_KEY => []
        );

        // Get formatted render, if required
        if(is_string($strRender))
        {
            $result = $strRender;

            // Run each template include patterns, if required
            if(count($tabTemplateIncPattern) > 0)
            {
                foreach($tabTemplateIncPattern as $strPattern)
                {
                    // Replace each template include matched expression, if required
                    $result = (
                    is_string($strPattern) ?
                        preg_replace_callback
                        (
                            $strPattern,
                            function (array $tabMatch) use (
                                $objRepository,
                                $tabConfig,
                                $formatCallable,
                                &$tabInfo
                            )
                            {
                                // Init var
                                $strKey = (isset($tabMatch[1]) ? $tabMatch[1] : null);

                                // Get replaced value
                                $result = $tabMatch[0];
                                if(!is_null($strKey))
                                {
                                    $result = $objRepository->getStrContent($strKey);
                                    $result = (
                                        (!is_null($formatCallable)) ?
                                            $formatCallable(
                                                $strKey,
                                                $result,
                                                $objRepository,
                                                $tabConfig,
                                                $tabInfo
                                            ) :
                                            $result
                                    );

                                    // Set info, if required
                                    if(!in_array($strKey, $tabInfo[ConstFormat::TAB_TMP_INC_INFO_KEY_INC_TEMPLATE_KEY]))
                                    {
                                        $tabInfo[ConstFormat::TAB_TMP_INC_INFO_KEY_INC_TEMPLATE_KEY][] = $strKey;
                                    }
                                }

                                // Return result
                                return $result;
                            },
                            $result
                        ) :
                        $result
                    );
                }
            }
        }

        // Return result
        return $result;
    }



}