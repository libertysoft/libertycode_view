<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\format\library;



class ConstFormat
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Template extension configuration
    const TAB_TMP_EXT_CONFIG_KEY_TEMPLATE_EXT_REGEXP = 'template_extend_regexp';
    const TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_REGEXP = 'dependency_declaration_regexp';
    const TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DCL_SPLIT = 'dependency_declaration_split';
    const TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DEF_REGEXP = 'dependency_definition_regexp';
    const TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_DEF_SPLIT = 'dependency_definition_split';
    const TAB_TMP_EXT_CONFIG_KEY_DEPENDENCY_PARENT_REGEXP = 'dependency_parent_regexp';
    const TAB_TMP_EXT_CONFIG_KEY_START_REGEXP = 'start_regexp';
    const TAB_TMP_EXT_CONFIG_KEY_END_REGEXP = 'end_regexp';

    // Template extension information configuration
    const TAB_TMP_EXT_INFO_KEY_PARENT_TEMPLATE_KEY = 'parent_template_key';
    const TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY = 'dependency_key';
    const TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DCL_NOT_FOUND = 'dependency_key_declaration_not_found';
    const TAB_TMP_EXT_INFO_KEY_DEPENDENCY_KEY_DEF_NOT_FOUND = 'dependency_key_definition_not_found';



    // Template inclusion configuration
    const TAB_TMP_INC_CONFIG_KEY_TEMPLATE_INC_REGEXP = 'template_include_regexp';
    const TAB_TMP_INC_CONFIG_KEY_TEMPLATE_CONTENT_FORMAT_CALLABLE = 'template_content_format_callable';

    // Template inclusion information configuration
    const TAB_TMP_INC_INFO_KEY_INC_TEMPLATE_KEY = 'include_template_key';



}