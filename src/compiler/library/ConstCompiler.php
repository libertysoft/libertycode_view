<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\library;



class ConstCompiler
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_CONFIG = 'tabConfig';
    const DATA_KEY_DEFAULT_CACHE_REPO = 'objCacheRepo';
    const DATA_KEY_DEFAULT_FORMAT_DATA_BEFORE = 'objFormatDataBefore';
    const DATA_KEY_DEFAULT_FORMAT_DATA_AFTER = 'objFormatDataAfter';



    // Configuration
    const TAB_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_CONFIG_KEY_CACHE_KEY_PATTERN = 'cache_key_pattern';
    const TAB_CONFIG_KEY_CACHE_SET_CONFIG = 'cache_set_config';
    const TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE = 'format_before_require';
    const TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE = 'format_after_require';

    // Configuration compile
    const TAB_COMPILE_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_COMPILE_CONFIG_KEY_FORMAT_BEFORE_REQUIRE = 'format_before_require';
    const TAB_COMPILE_CONFIG_KEY_FORMAT_AFTER_REQUIRE = 'format_after_require';



    // Exception message constants
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default compiler configuration standard.';
    const EXCEPT_MSG_CACHE_REPO_INVALID_FORMAT = 'Following cache repository "%1$s" invalid! It must be null or a cache repository object.';
    const EXCEPT_MSG_FORMAT_DATA_INVALID_FORMAT = 'Following format data "%1$s" invalid! It must be null or a format data object.';
    const EXCEPT_MSG_COMPILE_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array, following the default compiler compiling configuration standard.';
    const EXCEPT_MSG_RENDER_INVALID_FORMAT = 'Following render "%1$s" invalid! It must be a string.';



}