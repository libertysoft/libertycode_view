<?php
/**
 * Description :
 * This class allows to describe behavior of compiler class.
 * Compiler allows to get specific compiled render,
 * from specified render,
 * using specific compilation.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\api;



interface CompilerInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Methods getters
	// ******************************************************************************

    /**
     * Get specified string compiled render.
     *
     * Arguments array format:
     * Array of arguments can be provided,
     * to customize specified render.
     *
     * Configuration array format:
     * Compiling configuration can be provided,
     * to customize compilation.
     * Null means no specific compiled configuration required.
     *
     * @param string $strRender
     * @param array $tabArg = array()
     * @param array $tabConfig = null
     * @return string
     */
    public function getStrCompileRender(
        $strRender,
        array $tabArg = array(),
        array $tabConfig = null
    );



}