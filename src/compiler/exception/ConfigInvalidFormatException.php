<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\exception;

use Exception;

use liberty_code\view\compiler\library\ConstCompiler;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCompiler::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache required option
            (
                (!isset($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid cache key pattern
            (
                (!isset($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_KEY_PATTERN])) ||
                (
                    is_string($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_KEY_PATTERN]) &&
                    (trim($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_KEY_PATTERN]) != '')
                )
            ) &&

            // Check valid cache set configuration
            (
                (!isset($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_SET_CONFIG])) ||
                is_array($config[ConstCompiler::TAB_CONFIG_KEY_CACHE_SET_CONFIG])
            ) &&

            // Check valid format before required option
            (
                (!isset($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE]) ||
                    is_int($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE]) ||
                    (
                        is_string($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE]) &&
                        ctype_digit($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_BEFORE_REQUIRE])
                    )
                )
            ) &&

            // Check valid format after required option
            (
                (!isset($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE]) ||
                    is_int($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE]) ||
                    (
                        is_string($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE]) &&
                        ctype_digit($config[ConstCompiler::TAB_CONFIG_KEY_FORMAT_AFTER_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}