<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\compiler\exception;

use Exception;

use liberty_code\view\compiler\library\ConstCompiler;



class RenderInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $render
     */
	public function __construct($render)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstCompiler::EXCEPT_MSG_RENDER_INVALID_FORMAT, strval($render));
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified render has valid format.
	 * 
     * @param mixed $render
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($render)
    {
		// Init var
		$result = is_string($render);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($render);
		}
		
		// Return result
		return $result;
    }
	
	
	
}