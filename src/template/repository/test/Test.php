<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Load test
require_once($strRootAppPath . '/src/template/repository/test/TmpRepositoryTest.php');



// Init template repositories
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-1', 'Template 1');
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-2', 'Template 2');
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-3', 3);
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-cache-4', 'Template 4');
$objRegisterTmpRepo->getObjRegister()->putItem('tmp-key-cache-5', 'Template 5');

$objCacheTmpRepo->getObjRepository()->setItem('tmp-key-cache-5', 'Template cache 5');
$objCacheTmpRepo->getObjRepository()->setItem('tmp-key-cache-6', true);
$objCacheTmpRepo->getObjRepository()->setItem('tmp-key-cache-7', 'Template cache 7');



// Test config property
echo('Test register template repository config: <br />');

try{
    $objRegisterTmpRepo->setTabConfig(array('register_key_pattern' => 7));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test register template repository config: <pre>');var_dump($objRegisterTmpRepo->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



echo('Test cache template repository config: <br />');

try{
    $objCacheTmpRepo->setTabConfig(array('cache_key_pattern' => true));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test cache template repository config: <pre>');var_dump($objCacheTmpRepo->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



echo('Test multi template repository config: <br />');

try{
    $objMultiTmpRepo->setTabConfig(array('repository' => [false]));
} catch(\Exception $e) {
    echo($e->getMessage());
    echo('<br />');
}

echo('Test multi template repository config: <pre>');var_dump($objMultiTmpRepo->getTabConfig());echo('</pre>');

echo('<br /><br /><br />');



// Test template check, get content
$tabKey = array(
    'key-1', // Ok: found: "template 1"
    'key-3', // Ko: found, template content not valid
    'key cache 4', // Ko: not found
    'key-cache-4', // Ok: found: "Template 4"
    7, // Ko: bad key format
    'key-cache-5', // Ok: found: "Template cache 5"
    'key-cache-6', // Ko: found, template content not valid
    'key-cache-7' // Ok: found: "Template cache 7"

);

foreach($tabKey as $strKey)
{
    echo('Test check template, get template content"'.$strKey.'": <br />');
    try{
        echo('Check exists: <pre>');var_dump($objMultiTmpRepo->checkExists($strKey));echo('</pre>');
        echo('Get content: <pre>');var_dump($objMultiTmpRepo->getStrContent($strKey));echo('</pre>');
    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br />');
}

echo('<br /><br /><br />');



// Test cache
echo('Test cache: <br />');
echo('<pre>');print_r($objCacheRepo->getTabItem($objCacheRepo->getTabSearchKey()));echo('</pre>');
echo('<br />');

echo('<br /><br /><br />');


