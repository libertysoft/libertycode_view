<?php

// Init var
$strRootAppPath = dirname(__FILE__) . '/../../../..';

// Load external library
require_once($strRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strRootAppPath . '/include/Include.php');

// Use
use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\model\DefaultRepository;
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\view\template\repository\register\model\RegisterTmpRepository;
use liberty_code\view\template\repository\cache\model\CacheTmpRepository;
use liberty_code\view\template\repository\multi\model\MultiTmpRepository;



// Init DI
$objDepCollection = new DefaultDependencyCollection();
$objProvider = new DefaultProvider($objDepCollection);

// Init cache repository
$objCacheRepo = new DefaultRepository(
    null,
    new DefaultTableRegister()
);

// Init register template repository
$objRegisterTmpRepo = new RegisterTmpRepository(
    new DefaultTableRegister(),
    array(
        'cache_key_pattern' => 'tmp-saved-%1$s',
        'register_key_pattern' => 'tmp-%1$s'
    ),
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'source' => RegisterTmpRepository::class,
        'set' =>  ['type' => 'instance', 'value' => $objRegisterTmpRepo],
        'option' => [
            'shared' => true
        ]
    )));

// Init cache template repository
$objCacheTmpRepo = new CacheTmpRepository(
    new DefaultRepository(
        null,
        new DefaultTableRegister()
    ),
    array(
        'cache_key_pattern' => 'tmp-saved-%1$s',
        'repo_key_pattern' => 'tmp-%1$s'
    ),
    $objCacheRepo
);
$objProvider
    ->getObjDependencyCollection()
    ->setDependency(new Preference(array(
        'source' => CacheTmpRepository::class,
        'set' =>  ['type' => 'instance', 'value' => $objCacheTmpRepo],
        'option' => [
            'shared' => true
        ]
    )));

// Init multi template repository
$objMultiTmpRepo = new MultiTmpRepository(
    array(
        //'cache_key_pattern' => 'tmp-multi-saved-%1$s',
        'repository' => [
            $objRegisterTmpRepo,
            [
                'repository' => CacheTmpRepository::class,
                'select_key_prefix' => 'key-cache',
                'order' => -1
            ]
        ],
        //'select_repository_first_require' => true
    ),
    $objProvider,
    null//$objCacheRepo
);


