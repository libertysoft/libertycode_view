<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\multi\exception;

use Exception;

use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\template\repository\multi\library\ConstMultiTmpRepository;



class ConfigInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiTmpRepository::EXCEPT_MSG_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init string index array check function
        $checkTabStrIsValid = function(array $tabStr)
        {
            $result = true;
            $tabStr = array_values($tabStr);

            // Check each value is valid
            for($intCpt = 0; ($intCpt < count($tabStr)) && $result; $intCpt++)
            {
                $strValue = $tabStr[$intCpt];
                $result = (
                    // Check valid string
                    is_string($strValue) &&
                    (trim($strValue) != '')
                );
            }

            return $result;
        };

        // Init repository configuration array check function
        $checkTabRepoConfigIsValid = function($tabRepoConfig) use ($checkTabStrIsValid)
        {
            $result = is_array($tabRepoConfig) && (count($tabRepoConfig) > 0);

            // Check each repository configuration valid, if required
            if($result)
            {
                $tabRepoConfig = array_values($tabRepoConfig);
                for($intCpt = 0; ($intCpt < count($tabRepoConfig)) && $result; $intCpt++)
                {
                    $repoConfig = $tabRepoConfig[$intCpt];
                    $result = (
                        // Check valid repository
                        ($repoConfig instanceof TmpRepositoryInterface) ||

                        (
                            is_string($repoConfig) &&
                            (trim($repoConfig) != '')
                        ) ||

                        (
                            is_array($repoConfig) &&

                            // Check valid repository
                            isset($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY]) &&
                            (
                                ($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY] instanceof TmpRepositoryInterface) ||
                                (
                                    is_string($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY]) &&
                                    (trim($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY]) != '')
                                )
                            ) &&

                            // Check valid selection key REGEXP pattern
                            (
                                (!isset($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP])) ||
                                (
                                    is_string($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP]) &&
                                    (trim($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP]) != '')
                                )
                            ) &&

                            // Check valid selection key prefix
                            (
                                (!isset($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX])) ||
                                (
                                    is_string($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX]) &&
                                    (trim($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX]) != '')
                                ) ||
                                (
                                    is_array($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX]) &&
                                    $checkTabStrIsValid($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX])
                                )
                            ) &&

                            // Check valid order
                            (
                                (!isset($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER])) ||
                                is_int($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER])
                            )
                        )
                    );
                }
            }

            return $result;
        };

        // Init var
        $result =
            // Check valid repository
            isset($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY]) &&
            $checkTabRepoConfigIsValid($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY]) &&

            // Check valid select repository first required option
            (
                (!isset($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE]) ||
                    is_int($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE]) ||
                    (
                        is_string($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE]) &&
                        ctype_digit($config[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



	/**
	 * Check if specified config has valid format.
	 * 
     * @param mixed $config
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($config)
    {
		// Init var
		$result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($config) ? serialize($config) : $config));
		}
		
		// Return result
		return $result;
    }
	
	
	
}