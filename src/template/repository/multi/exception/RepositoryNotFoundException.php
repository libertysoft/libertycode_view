<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\multi\exception;

use Exception;

use liberty_code\view\template\repository\multi\library\ConstMultiTmpRepository;



class RepositoryNotFoundException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param string $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstMultiTmpRepository::EXCEPT_MSG_REPOSITORY_NOT_FOUND, strval($strKey));
	}



}