<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\multi\exception;

use Exception;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\view\template\repository\multi\library\ConstMultiTmpRepository;



class ProviderInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $provider
     */
	public function __construct($provider)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstMultiTmpRepository::EXCEPT_MSG_PROVIDER_INVALID_FORMAT,
            mb_strimwidth(strval($provider), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified provider has valid format.
	 * 
     * @param mixed $provider
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($provider)
    {
		// Init var
		$result = (
			(is_null($provider)) ||
			($provider instanceof ProviderInterface)
		);
		
		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($provider);
		}
		
		// Return result
		return $result;
    }
	
	
	
}