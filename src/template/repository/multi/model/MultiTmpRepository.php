<?php
/**
 * Description :
 * This class allows to define multi template repository class.
 * Multi template repository is default template repository,
 * using list of template repositories,
 * to load and get specified template content.
 *
 * Multi template repository uses the following specified configuration:
 * [
 *     Default template repository configuration,
 *
 *     repository(required): [
 *         // Template repository 1
 *         object template repository interface 1
 *
 *         OR
 *
 *         "string template repository dependency key|class path"
 *
 *         [
 *             repository(required):
 *                 object template repository interface 1
 *                 OR
 *                 "string template repository dependency key|class path",
 *
 *             select_key_regexp(optional: no REGEXP selection done, if not found):
 *                 "string REGEXP pattern,
 *                 used to select template keys, which match with following REGEXP pattern,
 *                 available for specific template repository",
 *
 *             select_key_prefix(optional: no prefix selection done, if not found):
 *                 "string prefix,
 *                 used to select template keys, which start with following prefix,
 *                 available for specific template repository"
 *                 OR
 *                 ["string prefix 1", ..., "string prefix N"],
 *
 *             order(optional: 0 got if not found): integer
 *         ],
 *
 *         ...,
 *
 *         // Template repository N
 *         ...
 *     ],
 *
 *     select_repository_first_require(optional: got false if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\multi\model;

use liberty_code\view\template\repository\model\DefaultTmpRepository;

use liberty_code\library\str\library\ToolBoxString;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\view\template\repository\library\ConstTmpRepository;
use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\template\repository\multi\library\ConstMultiTmpRepository;
use liberty_code\view\template\repository\multi\exception\ProviderInvalidFormatException;
use liberty_code\view\template\repository\multi\exception\ConfigInvalidFormatException;
use liberty_code\view\template\repository\multi\exception\RepositoryNotFoundException;



/**
 * @method null|ProviderInterface getObjProvider() Get provider object.
 * @method void setObjProvider(null|ProviderInterface $objProvider) Set provider object.
 */
class MultiTmpRepository extends DefaultTmpRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ProviderInterface $objProvider = null
     */
    public function __construct(
        array $tabConfig = null,
        ProviderInterface $objProvider = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );

        // Init provider
        $this->setObjProvider($objProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstMultiTmpRepository::DATA_KEY_DEFAULT_PROVIDER))
        {
            $this->__beanTabData[ConstMultiTmpRepository::DATA_KEY_DEFAULT_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstMultiTmpRepository::DATA_KEY_DEFAULT_PROVIDER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstMultiTmpRepository::DATA_KEY_DEFAULT_PROVIDER:
                    ProviderInvalidFormatException::setCheck($value);
                    break;

                case ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check select repository first option required,
     * used to select repository, on same order.
     *
     * @return boolean
     */
    public function checkSelectRepositoryFirstRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            isset($tabConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE]) &&
            (intval($tabConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_REPOSITORY_FIRST_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function checkExistsEngine($strKey)
    {
        // Init var
        $objRepository = $this->getObjRepository($strKey);
        $result = (!is_null($objRepository));

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of repository configurations.
     *
     * @param string $strKey = null
     * @param null|boolean $sortAsc = null
     * @return array
     */
    protected function getTabRepoConfig($strKey = null, $sortAsc = null)
    {
        // Init var
        $strKey = (is_string($strKey) ? $strKey : null);
        $boolSortAsc = (is_bool($sortAsc) ? $sortAsc : null);
        $tabConfig = $this->getTabConfig();
        $result = array_values($tabConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY]);

        // Filter repository configurations, if required
        if(!is_null($strKey))
        {
            $checkKeySelectedFromPrefix = function($strKey, $prefix)
            {
                $result = false;
                $tabPrefix = (is_array($prefix) ? array_values($prefix) : array($prefix));

                // Run each prefix
                for($intCpt = 0; ($intCpt < count($tabPrefix)) && (!$result); $intCpt++)
                {
                    // Check key starts with prefix
                    $strPrefix = $tabPrefix[$intCpt];
                    $result = ToolBoxString::checkStartsWith($strKey, $strPrefix);
                }

                return $result;
            };

            $result = array_filter(
                $result,
                function($repoConfig) use ($checkKeySelectedFromPrefix, $strKey) {
                    return (
                        (!is_array($repoConfig)) ||
                        (
                            (
                                (!array_key_exists(ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP, $repoConfig)) ||
                                (preg_match($repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_REGEXP], $strKey) == 1)
                            ) &&
                            (
                                (!array_key_exists(ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX, $repoConfig)) ||
                                $checkKeySelectedFromPrefix($strKey, $repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_SELECT_KEY_PREFIX])
                            )
                        )
                    );
                }
            );
        }

        // Sort repository configurations, if required
        if(is_bool($boolSortAsc))
        {
            usort(
                $result,
                function($repoConfig1, $repoConfig2)
                {
                    $intOrder1 = (
                        (
                            is_array($repoConfig1) &&
                            array_key_exists(ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER, $repoConfig1)
                        ) ?
                            $repoConfig1[ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $intOrder2 = (
                        (
                            is_array($repoConfig2) &&
                            array_key_exists(ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER, $repoConfig2)
                        ) ?
                            $repoConfig2[ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER] :
                            0
                    );
                    $result = (($intOrder1 < $intOrder2) ? -1 : (($intOrder1 > $intOrder2) ? 1 : 0));

                    return $result;
                }
            );

            // Get descending sort, if required
            if(!$boolSortAsc)
            {
                krsort($result);
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get repository object,
     * from specified string template key.
     *
     * @param string $strKey
     * @return null|TmpRepositoryInterface
     */
    protected function getObjRepository($strKey)
    {
        // Init var
        $result = null;
        $objProvider = $this->getObjProvider();
        $tabConfig = $this->getTabConfig();
        $tabRepoConfig = $this->getTabRepoConfig($strKey, true);
        $boolSelectFirst = $this->checkSelectRepositoryFirstRequired();

        // Run each repository configuration
        $intOrderRef = null;
        $boolOrderValid = true;
        for(
            $intCpt = 0;
            ($intCpt < count($tabRepoConfig)) &&
            (
                is_null($result) ||
                (!$boolSelectFirst)
            ) &&
            $boolOrderValid;
            $intCpt++
        )
        {
            // Get info
            $repoConfig = $tabRepoConfig[$intCpt];
            $intOrder = (
                (
                    is_array($repoConfig) &&
                    array_key_exists(ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER, $repoConfig)
                ) ?
                    $repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_ORDER] :
                    0
            );
            $boolOrderValid = (is_null($intOrderRef) || ($intOrderRef == $intOrder));

            // Get and register repository, if required
            if($boolOrderValid)
            {
                // Get repository
                $objRepository = (
                    (
                        is_array($repoConfig) &&
                        array_key_exists(ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY, $repoConfig)
                    ) ?
                        $repoConfig[ConstMultiTmpRepository::TAB_CONFIG_KEY_REPOSITORY] :
                        $repoConfig
                );
                $objRepository = (
                    (
                        is_string($objRepository) &&
                        (!is_null($objProvider))
                    ) ?
                        $objProvider->get($objRepository) :
                        $objRepository
                );

                // Check valid repository
                if(!($objRepository instanceof TmpRepositoryInterface))
                {
                    throw new ConfigInvalidFormatException(serialize($tabConfig));
                }

                // Register repository, if required
                if($objRepository->checkExists($strKey))
                {
                    // Init order reference, if required
                    $intOrderRef = (is_null($intOrderRef) ? $intOrder : $intOrderRef);

                    // Register repository
                    $result = $objRepository;
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws RepositoryNotFoundException
     */
    protected function getStrContentEngine($strKey)
    {
        // Init var
        $objRepository = $this->getObjRepository($strKey);

        // Check repository found
        if(is_null($objRepository))
        {
            throw new RepositoryNotFoundException($strKey);
        }

        // Return result
        return $objRepository->getStrContent($strKey);
    }



}