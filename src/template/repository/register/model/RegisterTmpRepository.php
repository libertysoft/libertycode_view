<?php
/**
 * Description :
 * This class allows to define register template repository class.
 * Register template repository is default template repository,
 * using register to load and get specified template content.
 *
 * Register template repository uses the following specified configuration:
 * [
 *     Default template repository configuration,
 *
 *     register_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on register, where '%1$s' replaced by specified template key"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\register\model;

use liberty_code\view\template\repository\model\DefaultTmpRepository;

use liberty_code\register\register\api\RegisterInterface;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\library\ConstTmpRepository;
use liberty_code\view\template\repository\exception\ContentInvalidException;
use liberty_code\view\template\repository\register\library\ConstRegisterTmpRepository;
use liberty_code\view\template\repository\register\exception\RegisterInvalidFormatException;
use liberty_code\view\template\repository\register\exception\ConfigInvalidFormatException;



/**
 * @method RegisterInterface getObjRegister() Get register object.
 * @method void setObjRegister(RegisterInterface $objRegister) Set register object.
 */
class RegisterTmpRepository extends DefaultTmpRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param RegisterInterface $objRegister
     */
    public function __construct(
        RegisterInterface $objRegister,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );

        // Init register
        $this->setObjRegister($objRegister);
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstRegisterTmpRepository::DATA_KEY_DEFAULT_REGISTER))
        {
            $this->__beanTabData[ConstRegisterTmpRepository::DATA_KEY_DEFAULT_REGISTER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstRegisterTmpRepository::DATA_KEY_DEFAULT_REGISTER
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstRegisterTmpRepository::DATA_KEY_DEFAULT_REGISTER:
                    RegisterInvalidFormatException::setCheck($value);
                    break;

                case ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkExistsEngine($strKey)
    {
        // Return result
        return $this
            ->getObjRegister()
            ->checkItemExists($this->getStrRegisterKey($strKey));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified template register key,
     * from specified template key.
     *
     * @param string $strKey
     * @return string
     */
    protected function getStrRegisterKey($strKey)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstRegisterTmpRepository::TAB_CONFIG_KEY_REGISTER_KEY_PATTERN, $tabConfig) ?
                sprintf($tabConfig[ConstRegisterTmpRepository::TAB_CONFIG_KEY_REGISTER_KEY_PATTERN], $strKey) :
                $strKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ContentInvalidException
     */
    protected function getStrContentEngine($strKey)
    {
        // Init var
        $result = $this
            ->getObjRegister()
            ->getItem($this->getStrRegisterKey($strKey));

        // Check valid content
        ContentInvalidException::setCheck($strKey, $result);

        // Return result
        return $result;
    }



}