<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\register\library;



class ConstRegisterTmpRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REGISTER = 'objRegister';



    // Configuration
    const TAB_CONFIG_KEY_REGISTER_KEY_PATTERN = 'register_key_pattern';



    // Exception message constants
    const EXCEPT_MSG_REGISTER_INVALID_FORMAT = 'Following register "%1$s" invalid! It must be a register object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the register template repository configuration standard.';



}