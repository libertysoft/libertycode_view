<?php
/**
 * Description :
 * This class allows to define default template repository class.
 * Can be consider is base of all template repository type.
 *
 * Default template repository uses the following specified configuration:
 * [
 *     cache_require(optional: got true if not found): true / false,
 *
 *     cache_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on cache repository, where '%1$s' replaced by specified template key",
 *
 *     cache_set_config(optional): [
 *         @see RepositoryInterface::setTabItem() configuration array format
 *     ]
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\model;

use liberty_code\library\bean\model\FixBean;
use liberty_code\view\template\repository\api\TmpRepositoryInterface;

use liberty_code\cache\repository\library\ToolBoxRepository;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\library\ConstTmpRepository;
use liberty_code\view\template\repository\exception\ConfigInvalidFormatException;
use liberty_code\view\template\repository\exception\CacheRepoInvalidFormatException;
use liberty_code\view\template\repository\exception\KeyInvalidFormatException;
use liberty_code\view\template\repository\exception\KeyNotFoundException;



/**
 * @method null|RepositoryInterface getObjCacheRepo() Get cache repository object.
 * @method void setTabConfig(array $tabConfig) Set config array.
 * @method void setObjCacheRepo(null|RepositoryInterface $objCacheRepo) Set cache repository object.
 */
abstract class DefaultTmpRepository extends FixBean implements TmpRepositoryInterface
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************
	
	/**
	 * @inheritdoc
	 * @param array $tabConfig = null
     * @param RepositoryInterface $objCacheRepo = null
     */
	public function __construct(
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
	{
		// Call parent constructor
		parent::__construct();

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setTabConfig($tabConfig);
        }

        // Init cache repository, if required
        if(!is_null($objCacheRepo))
        {
            $this->setObjCacheRepo($objCacheRepo);
        }
	}
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG))
        {
            $this->__beanTabData[ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG] = array();
        }

        if(!$this->beanExists(ConstTmpRepository::DATA_KEY_DEFAULT_CACHE_REPO))
        {
            $this->__beanTabData[ConstTmpRepository::DATA_KEY_DEFAULT_CACHE_REPO] = null;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG,
            ConstTmpRepository::DATA_KEY_DEFAULT_CACHE_REPO
        );
        $result = in_array($key, $tabKey);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    break;

                case ConstTmpRepository::DATA_KEY_DEFAULT_CACHE_REPO:
                    CacheRepoInvalidFormatException::setCheck($value);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabConfig()
    {
        // Return result
        return $this->beanGet(ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG);
    }



    /**
     * Check cache required.
     *
     * @return boolean
     */
    public function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!is_null($this->getObjCacheRepo())) &&
            (
                (!isset($tabConfig[ConstTmpRepository::TAB_CONFIG_KEY_CACHE_REQUIRE])) ||
                (intval($tabConfig[ConstTmpRepository::TAB_CONFIG_KEY_CACHE_REQUIRE]) != 0)
            )
        );

        // Return result
        return $result;
    }



    /**
     * Check if specified template exists engine.
     *
     * @param string $strKey
     * @return boolean
     */
    abstract protected function checkExistsEngine($strKey);



    /**
     * @inheritdoc
     * @throws KeyInvalidFormatException
     */
    public function checkExists($strKey)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Init var
        $result = (
            (
                $this->checkCacheRequired() &&
                $this
                    ->getObjCacheRepo()
                    ->checkItemExists($this->getStrCacheKey($strKey))
            ) ||
            $this->checkExistsEngine($strKey)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified template cache key,
     * from specified template key.
     *
     * @param string $strKey
     * @return string
     */
    protected function getStrCacheKey($strKey)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstTmpRepository::TAB_CONFIG_KEY_CACHE_KEY_PATTERN, $tabConfig) ?
                sprintf($tabConfig[ConstTmpRepository::TAB_CONFIG_KEY_CACHE_KEY_PATTERN], $strKey) :
                $strKey
        );

        // Return result
        return $result;
    }



    /**
     * Get specified string template content engine.
     *
     * @param string $strKey
     * @return string
     */
    abstract protected function getStrContentEngine($strKey);



    /**
     * @inheritdoc
     * @throws KeyNotFoundException
     */
    public function getStrContent($strKey)
    {
        // Set check argument
        if(!$this->checkExists($strKey))
        {
            throw new KeyNotFoundException($strKey);
        }

        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            $this->checkCacheRequired() ?
                ToolBoxRepository::getSetItem(
                    $this->getObjCacheRepo(),
                    $this->getStrCacheKey($strKey),
                    function() use ($strKey) {return $this->getStrContentEngine($strKey);},
                    (
                        isset($tabConfig[ConstTmpRepository::TAB_CONFIG_KEY_CACHE_SET_CONFIG]) ?
                            $tabConfig[ConstTmpRepository::TAB_CONFIG_KEY_CACHE_SET_CONFIG] :
                            null
                    )
                ) :
                $this->getStrContentEngine($strKey)
        );

        // Return result
        return $result;
    }



}