<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\exception;

use Exception;

use liberty_code\view\template\repository\library\ConstTmpRepository;



class ContentInvalidException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param string $strKey
     */
	public function __construct($strKey)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf(ConstTmpRepository::EXCEPT_MSG_CONTENT_INVALID_FORMAT, strval($strKey));
	}





    // Methods statics security (throw exception if check not pass)
    // ******************************************************************************

    /**
     * Check if specified content has valid format.
     *
     * @param string $strKey
     * @param mixed $content
     * @return boolean
     * @throws static
     */
    public static function setCheck($strKey, $content)
    {
        // Init var
        $result = is_string($content);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($strKey);
        }

        // Return result
        return $result;
    }



}