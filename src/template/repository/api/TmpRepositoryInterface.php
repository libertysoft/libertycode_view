<?php
/**
 * Description :
 * This class allows to describe behavior of template repository class.
 * Template repository contains all information to prepare specified template,
 * to load and get its content.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\api;



interface TmpRepositoryInterface
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified template exists.
     *
     * @param string $strKey
     * @return boolean
     */
    public function checkExists($strKey);





	// Methods getters
	// ******************************************************************************

    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig();



    /**
     * Get specified string template content.
     *
     * @param string $strKey
     * @return string
     */
    public function getStrContent($strKey);



}