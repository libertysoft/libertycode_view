<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\cache\library;



class ConstCacheTmpRepository
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_REPOSITORY = 'objRepository';



    // Configuration
    const TAB_CONFIG_KEY_REPO_KEY_PATTERN = 'repo_key_pattern';



    // Exception message constants
    const EXCEPT_MSG_REPOSITORY_INVALID_FORMAT = 'Following repository "%1$s" invalid! It must be a repository object.';
    const EXCEPT_MSG_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the cache template repository configuration standard.';



}