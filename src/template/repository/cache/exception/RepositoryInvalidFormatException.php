<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\cache\exception;

use Exception;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\cache\library\ConstCacheTmpRepository;



class RepositoryInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $repository
     */
	public function __construct($repository)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstCacheTmpRepository::EXCEPT_MSG_REPOSITORY_INVALID_FORMAT,
            mb_strimwidth(strval($repository), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified repository has valid format.
	 * 
     * @param mixed $repository
	 * @return boolean
	 * @throws static
     */
	public static function setCheck($repository)
    {
		// Init var
		$result = (
		    (!is_null($repository)) &&
            ($repository instanceof RepositoryInterface)
        );

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static($repository);
		}
		
		// Return result
		return $result;
    }
	
	
	
}