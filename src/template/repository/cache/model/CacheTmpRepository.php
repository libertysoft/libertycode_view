<?php
/**
 * Description :
 * This class allows to define cache template repository class.
 * Cache template repository is default template repository,
 * using cache repository to load and get specified template content.
 *
 * Cache template repository uses the following specified configuration:
 * [
 *     Default template repository configuration,
 *
 *     repo_key_pattern(optional):
 *         "string sprintf pattern,
 *         to build key used on repository, where '%1$s' replaced by specified template key"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\view\template\repository\cache\model;

use liberty_code\view\template\repository\model\DefaultTmpRepository;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\view\template\repository\library\ConstTmpRepository;
use liberty_code\view\template\repository\exception\ContentInvalidException;
use liberty_code\view\template\repository\cache\library\ConstCacheTmpRepository;
use liberty_code\view\template\repository\cache\exception\RepositoryInvalidFormatException;
use liberty_code\view\template\repository\cache\exception\ConfigInvalidFormatException;



/**
 * @method RepositoryInterface getObjRepository() Get repository object.
 * @method void setObjRepository(RepositoryInterface $objRepository) Set repository object.
 */
class CacheTmpRepository extends DefaultTmpRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

	// Constructor / Destructor / Others
	// ******************************************************************************

    /**
     * @inheritdoc
     * @param RepositoryInterface $objRepository
     */
    public function __construct(
        RepositoryInterface $objRepository,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $tabConfig,
            $objCacheRepo
        );

        // Init repository
        $this->setObjRepository($objRepository);
    }
	
	
	
	
	
    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstCacheTmpRepository::DATA_KEY_DEFAULT_REPOSITORY))
        {
            $this->__beanTabData[ConstCacheTmpRepository::DATA_KEY_DEFAULT_REPOSITORY] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstCacheTmpRepository::DATA_KEY_DEFAULT_REPOSITORY
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstCacheTmpRepository::DATA_KEY_DEFAULT_REPOSITORY:
                    RepositoryInvalidFormatException::setCheck($value);
                    break;

                case ConstTmpRepository::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkExistsEngine($strKey)
    {
        // Return result
        return $this
            ->getObjRepository()
            ->checkItemExists($this->getStrRepoKey($strKey));
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get specified template repository key,
     * from specified template key.
     *
     * @param string $strKey
     * @return string
     */
    protected function getStrRepoKey($strKey)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            array_key_exists(ConstCacheTmpRepository::TAB_CONFIG_KEY_REPO_KEY_PATTERN, $tabConfig) ?
                sprintf($tabConfig[ConstCacheTmpRepository::TAB_CONFIG_KEY_REPO_KEY_PATTERN], $strKey) :
                $strKey
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     * @throws ContentInvalidException
     */
    protected function getStrContentEngine($strKey)
    {
        // Init var
        $result = $this
            ->getObjRepository()
            ->getItem($this->getStrRepoKey($strKey));

        // Check valid content
        ContentInvalidException::setCheck($strKey, $result);

        // Return result
        return $result;
    }



}