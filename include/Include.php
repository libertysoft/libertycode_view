<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/template/repository/library/ConstTmpRepository.php');
include($strRootPath . '/src/template/repository/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/template/repository/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/template/repository/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/template/repository/exception/KeyNotFoundException.php');
include($strRootPath . '/src/template/repository/exception/ContentInvalidException.php');
include($strRootPath . '/src/template/repository/api/TmpRepositoryInterface.php');
include($strRootPath . '/src/template/repository/model/DefaultTmpRepository.php');

include($strRootPath . '/src/template/repository/register/library/ConstRegisterTmpRepository.php');
include($strRootPath . '/src/template/repository/register/exception/RegisterInvalidFormatException.php');
include($strRootPath . '/src/template/repository/register/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/template/repository/register/model/RegisterTmpRepository.php');

include($strRootPath . '/src/template/repository/cache/library/ConstCacheTmpRepository.php');
include($strRootPath . '/src/template/repository/cache/exception/RepositoryInvalidFormatException.php');
include($strRootPath . '/src/template/repository/cache/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/template/repository/cache/model/CacheTmpRepository.php');

include($strRootPath . '/src/template/repository/multi/library/ConstMultiTmpRepository.php');
include($strRootPath . '/src/template/repository/multi/exception/ProviderInvalidFormatException.php');
include($strRootPath . '/src/template/repository/multi/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/template/repository/multi/exception/RepositoryNotFoundException.php');
include($strRootPath . '/src/template/repository/multi/model/MultiTmpRepository.php');

include($strRootPath . '/src/compiler/format/library/ConstFormatData.php');
include($strRootPath . '/src/compiler/format/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/compiler/format/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/compiler/format/exception/ValueInvalidFormatException.php');
include($strRootPath . '/src/compiler/format/model/FormatData.php');

include($strRootPath . '/src/compiler/format/library/ConstFormat.php');
include($strRootPath . '/src/compiler/format/library/ToolBoxFormatTmpExtension.php');
include($strRootPath . '/src/compiler/format/library/ToolBoxFormatTmpInclusion.php');

include($strRootPath . '/src/compiler/library/ConstCompiler.php');
include($strRootPath . '/src/compiler/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/compiler/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/compiler/exception/FormatDataInvalidFormatException.php');
include($strRootPath . '/src/compiler/exception/CompileConfigInvalidFormatException.php');
include($strRootPath . '/src/compiler/exception/RenderInvalidFormatException.php');
include($strRootPath . '/src/compiler/api/CompilerInterface.php');
include($strRootPath . '/src/compiler/model/DefaultCompiler.php');

include($strRootPath . '/src/compiler/php/library/ConstPhpCompiler.php');
include($strRootPath . '/src/compiler/php/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/compiler/php/exception/CompileConfigInvalidFormatException.php');
include($strRootPath . '/src/compiler/php/exception/ArgInvalidFormatException.php');
include($strRootPath . '/src/compiler/php/model/PhpCompiler.php');

include($strRootPath . '/src/viewer/library/ConstViewer.php');
include($strRootPath . '/src/viewer/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/viewer/exception/CacheRepoInvalidFormatException.php');
include($strRootPath . '/src/viewer/exception/DataInvalidFormatException.php');
include($strRootPath . '/src/viewer/exception/RenderConfigInvalidFormatException.php');
include($strRootPath . '/src/viewer/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/viewer/exception/KeyNotFoundException.php');
include($strRootPath . '/src/viewer/api/ViewerInterface.php');
include($strRootPath . '/src/viewer/model/DefaultViewer.php');

include($strRootPath . '/src/viewer/standard/library/ConstStandardViewer.php');
include($strRootPath . '/src/viewer/standard/exception/TemplateRepoInvalidFormatException.php');
include($strRootPath . '/src/viewer/standard/exception/CompilerInvalidFormatException.php');
include($strRootPath . '/src/viewer/standard/exception/RenderConfigInvalidFormatException.php');
include($strRootPath . '/src/viewer/standard/model/StandardViewer.php');

include($strRootPath . '/src/viewer/multi/library/ConstMultiViewer.php');
include($strRootPath . '/src/viewer/multi/exception/ProviderInvalidFormatException.php');
include($strRootPath . '/src/viewer/multi/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/viewer/multi/exception/ViewerNotFoundException.php');
include($strRootPath . '/src/viewer/multi/model/MultiViewer.php');

include($strRootPath . '/src/view/library/ConstView.php');
include($strRootPath . '/src/view/exception/ViewerInvalidFormatException.php');
include($strRootPath . '/src/view/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/view/api/ViewInterface.php');
include($strRootPath . '/src/view/model/DefaultView.php');

include($strRootPath . '/src/view/factory/library/ConstViewFactory.php');
include($strRootPath . '/src/view/factory/exception/FactoryInvalidFormatException.php');
include($strRootPath . '/src/view/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/view/factory/api/ViewFactoryInterface.php');
include($strRootPath . '/src/view/factory/model/DefaultViewFactory.php');

include($strRootPath . '/src/view/factory/standard/library/ConstStandardViewFactory.php');
include($strRootPath . '/src/view/factory/standard/model/StandardViewFactory.php');