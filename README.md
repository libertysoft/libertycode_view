LibertyCode_View
================



Description
-----------

Library contains view components, 
to manage rendering.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/view ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/view": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Template repository

Template repository allows to manage and prepare templates, 
to load and get template contents,
from specific storage support,
from specific configuration.

_Elements_

- TmpRepository
    
    Allows to design a template repository,
    which contains all information to prepare specified template,
    to load and get its content.
    
- RegisterTmpRepository

    Extends template repository features.
    It uses register features, 
    to load and get specified template content.

- CacheTmpRepository

    Extends template repository features.
    It uses cache repository features, 
    to load and get specified template content.
    
- MultiTmpRepository
    
    Extends template repository features.
    It uses list of template repositories,
    to load and get specified template content.
    
_Example_

```php
// Get register
use liberty_code\register\register\memory\model\MemoryRegister;
$register = new MemoryRegister();
...
// Get repository
use liberty_code\view\template\repository\register\model\RegisterTmpRepository;
$repository = new RegisterTmpRepository(
    $register
);
...
// Get template content, if required
if($repository->checkExists('template_key'))
{
    var_dump($repository->getStrContent('template_key'));
}
...
```

#### Compiler format data

Simple array data allows to manage formatting render, during compiling.

#### Compiler format utility

Formatting render utilities, can be used during compiling.

_Elements_

- FormatTmpExtension 

    Allows to get specified formatted render,
    using template extension feature.
    
- FormatTmpInclusion 

    Allows to get specified formatted render,
    using template inclusion feature.

#### Compiler

Compiler allows to get compiled render, 
using specific compilation.

_Elements_

- Compiler
    
    Allows to design a compiler,
    which allows to get specific compiled render,
    from specified render,
    using specific compilation.

- PhpCompiler
    
    Extends compiler features.
    It uses PHP compilation, 
    to get specific compiled render.
    
- StandardPhpCompiler
    
    Extends PHP compiler features.
    It uses standard formatting and features, 
    to get specific compiled render.

_Example_

```php
// Get compiler
use liberty_code\view\compiler\php\model\PhpCompiler;
$compiler = new PhpCompiler();
...
// Get compiled render
var_dump($compiler->getStrCompileRender(
    '...render to compile', 
    ['arg1' => mixed arg value 1, ..., 'argN' => mixed arg value N]
));
... 
```

#### Viewer

Viewer allows to get render.

_Elements_

- Viewer
    
    Allows to design a viewer,
    which allows to get specified render.
    
- StandardViewer
    
    Extends viewer features.
    It uses template repository and compiler, 
    to get specified render.

- MultiViewer
    
    Extends viewer features.
    It uses list of viewers,
    to get specified render.
    
_Example_

```php
// Get viewer
use liberty_code\view\viewer\standard\model\StandardViewer;
$viewer = new StandardViewer(
    $repository
    $compiler
);
...
// Get render, if required
if($viewer->checkExists('render_key'))
{
    var_dump($viewer->getStrRender(
        'render_key',
        ['arg1' => mixed arg value 1, ..., 'argN' => mixed arg value N]
    ));
}
...
```

#### View

View allows to get specific render.

_Elements_

- View
    
    Allows to design a view,
    which contains all information, 
    to get specific render,
    using specific viewer.
    
- ViewFactory

    Allows to design a view factory, 
    to provide new or specified view instance,
    from specified configuration.

- StandardViewFactory

    Extends view factory features. 
    Provides view instance.
    
_Example_

```php
// Get view factory
use liberty_code\view\view\factory\standard\model\StandardViewFactory;
$viewFactory = new StandardViewFactory($viewer);
...
// Get new view from configuration
$view = $viewFactory->getObjView(array(...));
...
// Get render, if required
if($view->checkExists())
{
    var_dump($view->getStrRender());
}
...
```

---


